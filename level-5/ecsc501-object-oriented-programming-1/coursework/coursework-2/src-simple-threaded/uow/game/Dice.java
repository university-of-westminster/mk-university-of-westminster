package uow.game;

import java.io.FileNotFoundException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import java.util.ArrayList;
import java.util.Random;

import uow.gui.DiceActionListener;
import uow.game.Die;
import uow.game.DiceBot;

import uow.thread.DiceWorker;

public class Dice{
	private ArrayList<Die> dies;
	private Random rng;

	public DiceBot bot;

	private ArrayList<ArrayList<Die>> currentDices;
	public ArrayList<Integer> score;
	public int state;

	private JFrame window;
	private JPanel top;
	private JPanel left;
	private JPanel right;
	private JPanel bottom;
	private ArrayList<JButton> imagesLeft;
	private ArrayList<JButton> imagesRight;
	private ArrayList<JCheckBox> cbsRight;
	private JButton btnThrow;
	private JButton btnScore;
	private JButton btnReset;
	private JLabel lblScore;
	private JLabel lblWins;
	private JLabel lblInterim;

	private ImageIcon iconNone;

	public int winThreshold;

	public static final String SCORE_FILE = "score.dat";
	public static final String ICON_PATH = "img/";

	public static final int ID_PLAYER = 0;
	public static final int ID_BOT    = 1;

	public static final int IMAGES_LEFT  = 1;
	public static final int IMAGES_RIGHT = 2;

	public static final int DICES = 5;

	public static final int MAX_REROLL_DICES = 5;
	public static final int DEFAULT_WIN_THRESHOLD = 101;

	public static final int STATE_ROLL_ONE      = 1;
	public static final int STATE_ROLL_TWO      = 2;
	public static final int STATE_ROLL_THREE    = 3;
	public static final int STATE_ROLL_DRAW     = 4;
	public static final int STATE_ROLL_FINISHED = 5;
	public static final int STATE_END = 9;

	public Dice(){
		this.dies = new ArrayList<Die>();
		for (int i = 0; i < 6; i++){
			this.dies.add(new Die(i+1));
		}

		this.score = new ArrayList<Integer>();
		this.score.add(0); this.score.add(0);

		this.currentDices = new ArrayList<ArrayList<Die>>();
		this.currentDices.add(new ArrayList<Die>());
		this.currentDices.add(new ArrayList<Die>());
		for (int i = 0; i < Dice.DICES; i++){
			this.currentDices.get(Dice.ID_PLAYER).add(new Die());
			this.currentDices.get(Dice.ID_BOT).add(new Die());
		}


		this.state = Dice.STATE_ROLL_ONE;

		this.rng = new Random();
		this._initGUI();
		this._initHooks();

		this.bot = new DiceBot(this);

		this.winThreshold = Dice.DEFAULT_WIN_THRESHOLD;
		Object tmp = JOptionPane.showInputDialog(
			this.window,
			"Winning score threshold:",
			"Initial configuration",
			JOptionPane.QUESTION_MESSAGE,
			null,
			null,
			Dice.DEFAULT_WIN_THRESHOLD
		);
		if (tmp != null){
			try{
				this.winThreshold = Integer.parseInt(((String)tmp));
			} catch (NumberFormatException e){}
		}

		this._reset(false);
	}

	public ArrayList<Die> roll(){
		return this.roll(Dice.DICES);
	}

	public ArrayList<Die> roll(int num){
		ArrayList<Die> ret = new ArrayList<Die>();
		for (int i = 0; i < num; i++){
			ret.add(this.dies.get(this.rng.nextInt(6)));
			//ret.add((this.state == Dice.STATE_ROLL_DRAW)?(this.dies.get(this.rng.nextInt(6))):(this.dies.get(5)));
		}
		return ret;
	}

	public void reroll(int playerId, ArrayList<Boolean> kept){
		int rerolled = 0;
		for (int i = 0; i < Dice.DICES; i++){
			if (!kept.get(i)){
				this.currentDices.get(playerId).set(i, this.roll(1).get(0));
				if (playerId == Dice.ID_PLAYER){
					this.imagesRight.get(i).setIcon(this.currentDices.get(Dice.ID_PLAYER).get(i).getDieImage());
				} else{
					this.imagesLeft.get(i).setIcon(this.currentDices.get(Dice.ID_BOT).get(i).getDieImage());
				}
				rerolled++;
			}
		}
		if (rerolled > Dice.MAX_REROLL_DICES){
			System.out.println("FATAL: Caught cheater.");
			System.out.println("FATAL: Player ID [" + playerId + "] has been found to be cheating.");
			System.out.println("FATAL: Game will now exit.");
			System.exit(1);
		}
	}

	public void rerollRaw(int playerId, ArrayList<Boolean> kept){
		int rerolled = 0;
		for (int i = 0; i < Dice.DICES; i++){
			if (!kept.get(i)){
				this.currentDices.get(playerId).set(i, this.roll(1).get(0));
				rerolled++;
			}
		}
		if (rerolled > Dice.MAX_REROLL_DICES){
			System.out.println("FATAL: Caught cheater.");
			System.out.println("FATAL: Player ID [" + playerId + "] has been found to be cheating.");
			System.out.println("FATAL: Game will now exit.");
			System.exit(1);
		}
	}

	public void reroll(int playerId){
		ArrayList<Boolean> allFalse = new ArrayList<Boolean>();
		for (int i = 0; i < Dice.DICES; i++){ allFalse.add(false); }
		this.reroll(playerId, allFalse);
	}

	public void rerollRaw(int playerId){
		ArrayList<Boolean> allFalse = new ArrayList<Boolean>();
		for (int i = 0; i < Dice.DICES; i++){ allFalse.add(false); }
		this.rerollRaw(playerId, allFalse);
	}


	public void executeThrow(){
		System.out.println("DEBUG: Throwing for state" + this.state);
		switch (this.state){
			case Dice.STATE_ROLL_THREE:
			case Dice.STATE_ROLL_TWO:
				/*
				 * THREADING
				 *
				 * We could make our two threads work together using process() or other means
				 * so we don't block the event thread by calling get() on the threads inside it.
				 * 
				 * Or we can spawn a new thread in the event thread and create the player and
				 * bot threads from within this new thread making our event thread fully
				 * responsive while being able to wait for the bot a computer worker threads to
				 * complete
				 *
				 * (The above is my understanding of the whole threading principle, it might be wrong)
				 *
				 * Second option is way easier to implement and delivers basically the same results
				 * so that is how all threading is implemented in this application
				 */
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;

						tPlayer = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.reroll(Dice.ID_PLAYER, this.dice.getSelected());
								return null;
							}
						};
						tBot = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.bot.play();
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}

						this.dice.state = (this.dice.state == Dice.STATE_ROLL_TWO)?(Dice.STATE_ROLL_THREE):(Dice.STATE_ROLL_FINISHED);
						return null;
					}
					@Override
					public void done(){
						this.dice.displayUpdate();
						this.dice.displayInterimScore();
						if (this.dice.state == Dice.STATE_ROLL_FINISHED){ this.dice.executeScore(); }
						else{ this.dice.enableControls(true, true, true, true); }
					}
				}).execute();
				break;
			case Dice.STATE_ROLL_ONE:
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;

						tPlayer = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.rerollRaw(Dice.ID_PLAYER);
								return null;
							}
						};
						tBot = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.bot.play();
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}

						return null;
					}
					@Override
					public void done(){
						this.dice.state = Dice.STATE_ROLL_TWO;
						this.dice.displayInterimScore();
						this.dice.displayUpdate();
						this.dice.enableControls(true, true, true, true);
						this.dice.checkboxHook();
						
					}
				}).execute();
				break;
			case Dice.STATE_ROLL_DRAW:
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;

						tPlayer = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.rerollRaw(Dice.ID_PLAYER);
								return null;
							}
						};
						tBot = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.bot.play();
								this.dice.bot.reset();
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}

						return null;
					}
					@Override
					public void done(){
						this.dice.displayUpdate();
						this.dice.displayInterimScore();
						this.dice.executeScore();
					}
				}).execute();
		}
	}

	public void executeScore(){
		switch (this.state){
			case Dice.STATE_ROLL_DRAW:
			case Dice.STATE_ROLL_FINISHED:
			case Dice.STATE_ROLL_ONE:
			case Dice.STATE_ROLL_TWO:
			case Dice.STATE_ROLL_THREE:
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;
						
						tPlayer = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.scoreCurrentRoll(Dice.ID_PLAYER);
								return null;
							}
						};
						tBot = new DiceWorker(this.dice){
							@Override
							public Void doInBackground(){
								this.dice.bot.finish();
								this.dice.scoreCurrentRoll(Dice.ID_BOT);
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}
						
						return null;
					}
					@Override
					public void done(){
						this.dice.displayScore();
						this.dice.resetDisplay(false);
						this.dice.checkWin();
						if (this.dice.state == Dice.STATE_ROLL_DRAW){
							this.dice.enableControls(true, false, true, false);
						} else{ this.dice.state = Dice.STATE_ROLL_ONE; }
					}
				}).execute();
		}
	}

	public void executeReset(){
		this._reset(true);
	}


	public ArrayList<Boolean> getSelected(){
		ArrayList<Boolean> ret = new ArrayList<Boolean>();
		for (int i = 0; i < Dice.DICES; i++){
			ret.add(this.cbsRight.get(i).isSelected());
		}
		return ret;
	}

	public boolean checkWin(){
		if ((this.score.get(Dice.ID_PLAYER).intValue() >= this.winThreshold) || (this.score.get(Dice.ID_BOT).intValue() >= this.winThreshold)){
			if (this.score.get(Dice.ID_PLAYER).equals(this.score.get(Dice.ID_BOT))){
				this.state = Dice.STATE_ROLL_DRAW;
				JOptionPane.showMessageDialog(
					this.window,
					"It's a draw! Roll once.",
					"Draw!",
					JOptionPane.PLAIN_MESSAGE
				);
				return false;
			} else{
				boolean playerWon = (this.score.get(Dice.ID_PLAYER).intValue() > this.score.get(Dice.ID_BOT).intValue())?(true):(false);
				JOptionPane.showMessageDialog(
					this.window,
					"Winner: " + ((playerWon)?("PLAYER"):("COMPUTER")),
					"Game has ended",
					(playerWon)?(JOptionPane.WARNING_MESSAGE):(JOptionPane.ERROR_MESSAGE)
				);
				if (playerWon){
					this._addWin();
				} else{
					this._addLoss();
				}
				this._reset(false);
				return true;
			}
		}
		return false;
	}

	private int _getCurrentRollScore(int playerId){ //
		int ret = 0;
		for (int i = 0; i < Dice.DICES; i++){
			ret += this.currentDices.get(playerId).get(i).getValue();
		}
		return ret;
	}

	public void displayUpdate(){
		for (int i = 0; i < Dice.DICES; i++){
			this.imagesLeft.get(i).setIcon(this.currentDices.get(Dice.ID_BOT).get(i).getDieImage());
			this.imagesRight.get(i).setIcon(this.currentDices.get(Dice.ID_PLAYER).get(i).getDieImage());
		}
	}

	public void resetDisplay(boolean resetDiceDisplay){
		if (resetDiceDisplay){
			for (int i = 0; i < Dice.DICES; i++){
				this.imagesLeft.get(i).setIcon(this.iconNone);
				this.imagesRight.get(i).setIcon(this.iconNone);
				this.currentDices.get(Dice.ID_PLAYER).set(i, null);
				this.currentDices.get(Dice.ID_BOT).set(i, null);
			}
		}
		this.lblInterim.setText("0|0");
		this.enableControls(true, false, true, false);
		this._resetCheckboxes();
	}

	private void _reset(boolean confirm){
		/*
		if (confirm){
			if (JOptionPane.showConfirmDialog(this.window, "Do you really want to reset the game?", "Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
		*/
				this.score.set(this.ID_PLAYER, 0);
				this.score.set(this.ID_BOT, 0);
				this.resetDisplay(true);
				this.state = Dice.STATE_ROLL_ONE;
				this.bot.reset();
				this.lblScore.setText("0|0");
				this._setWins();
			/* }
		} */
	}

	public void enableControls(boolean t, boolean s, boolean r, boolean c){
		this.btnThrow.setEnabled(t);
		this.btnScore.setEnabled(s);
		this.btnReset.setEnabled(r);
		for (int i = 0; i < Dice.DICES; i++){
			this.cbsRight.get(i).setEnabled(c);
		}
	}

	private void _resetCheckboxes(){
		for (int i = 0; i < Dice.DICES; i++){
			this.cbsRight.get(i).setSelected(false);
		}
	}

	public void scoreCurrentRoll(int playerId){
		for (int i = 0; i < Dice.DICES; i++){
			this.score.set(playerId, this.score.get(playerId) + this.currentDices.get(playerId).get(i).getValue());
		}
	}

	public void displayScore(){
		this.lblScore.setText(this.score.get(Dice.ID_BOT) + "|" + this.score.get(Dice.ID_PLAYER));
	}

	public void displayInterimScore(){
		int interimPlayer = this.score.get(Dice.ID_PLAYER);
		int interimBot = this.score.get(Dice.ID_BOT);
		for (int i = 0; i < Dice.DICES; i++){
			interimPlayer += this.currentDices.get(Dice.ID_PLAYER).get(i).getValue();
			interimBot += this.currentDices.get(Dice.ID_BOT).get(i).getValue();

		}
		this.lblInterim.setText(interimBot + "|" + interimPlayer);
	}

	public void checkboxHook(){
		int total = 0;
		for (int i = 0; i < Dice.DICES; i++){
			if (this.cbsRight.get(i).isSelected()){ total++; }
		}
		if (total == Dice.DICES){
			this.btnThrow.setEnabled(false);
		} else{
			this.btnThrow.setEnabled(((Dice.DICES - total) <= Dice.MAX_REROLL_DICES)?(true):(false));
		}
	}

	private void _initGUI(){
		// Initialize resources
		this.imagesLeft = new ArrayList<JButton>();
		this.imagesRight = new ArrayList<JButton>();
		this.cbsRight = new ArrayList<JCheckBox>();
		this.lblScore = new JLabel("0|0");
		this.lblWins = new JLabel("0/0");
		this.lblInterim = new JLabel("0|0");
		this.iconNone = new ImageIcon(Dice.ICON_PATH + "none.png");

		// Initialize components
		this.window = new JFrame("Dice Game");
		this.top = new JPanel();
		this.left = new JPanel();
		this.right = new JPanel();
		this.bottom = new JPanel();
		this.btnThrow = new JButton("Throw");
		this.btnScore = new JButton("Score");
		this.btnReset = new JButton("Restart Game");

		for (int i =0; i < Dice.DICES; i++){
			this.imagesLeft.add(new JButton(this.iconNone));
			this.imagesRight.add(new JButton(this.iconNone));

			this.cbsRight.add(new JCheckBox("keep"));
		}

		// Setup styling
		this.window.getContentPane().setLayout(new BorderLayout());
		this.top.setLayout(new GridLayout(0, 3));
		this.left.setLayout(new GridLayout(0,2));
		this.right.setLayout(new GridLayout(0,2));
		for (int i = 0; i < Dice.DICES; i++){
			JButton tmp = this.imagesLeft.get(i);
			tmp.setBorderPainted(false);
			tmp.setFocusPainted(false);
			tmp.setContentAreaFilled(false);

			tmp = this.imagesRight.get(i);
			tmp.setBorderPainted(false);
			tmp.setFocusPainted(false);
			tmp.setContentAreaFilled(false);
		}
		this.lblScore.setHorizontalAlignment(JLabel.CENTER);
		this.lblWins.setHorizontalAlignment(JLabel.CENTER);
		this.lblInterim.setHorizontalAlignment(JLabel.CENTER);

		// Containerize
		for (int i = 0; i < Dice.DICES; i++){
			JLabel tmp = new JLabel("Dice " + (i+1));
			tmp.setHorizontalAlignment(JLabel.CENTER);
			this.left.add(tmp);
			this.left.add(this.imagesLeft.get(i));
			this.right.add(this.imagesRight.get(i));
			this.right.add(this.cbsRight.get(i));
		}
		JLabel tmp = new JLabel("SCORE");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		tmp = new JLabel("WINS/LOSSES");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		tmp = new JLabel("INTERIM");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		this.top.add(this.lblScore);
		this.top.add(this.lblWins);
		this.top.add(this.lblInterim);
		this.bottom.add(this.btnThrow);
		this.bottom.add(this.btnScore);
		this.bottom.add(this.btnReset);
		this.window.add(this.top, "North");
		this.window.add(this.left, "West");
		this.window.add(this.right, "East");
		this.window.add(this.bottom, "South");

		// Final init
		this.window.setMinimumSize(new Dimension(450, 100 + (Dice.DICES * 110)));
		this.window.setPreferredSize(new Dimension(725, 600));
		this.window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.window.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				System.out.println("Exiting... [def.WindowAdapter]");
				System.exit(0);
			}
		});
		this.window.pack();
		this.window.setLocation(
			Toolkit.getDefaultToolkit().getScreenSize().width/2-this.window.getSize().width/2,
			Toolkit.getDefaultToolkit().getScreenSize().height/2-this.window.getSize().height/2
		);
		this.window.setVisible(true);
	}

	private void _initHooks(){
		this.btnThrow.addActionListener(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.dice.executeThrow();
			}
		});

		this.btnScore.addActionListener(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.dice.executeScore();
			}
		});

		this.btnReset.addActionListener(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.dice.executeReset();
			}
		});

		DiceActionListener tmp = new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.dice.checkboxHook();
			}
		};
		for (int i = 0; i < Dice.DICES; i++){
			this.cbsRight.get(i).addActionListener(tmp);
		}
	}


	private void _setWins(){
		ArrayList<Integer> score = this._getWins();
		this.lblWins.setText(score.get(0) + "|" + score.get(1));
	}

	private void _checkFile(){
		File f = new File(Dice.SCORE_FILE);
		if (!f.exists()){
			try{
				PrintWriter w = new PrintWriter(Dice.SCORE_FILE);
				w.println("0");
				w.println("0");
				w.close();
			} catch (FileNotFoundException e){
				System.out.println("FATAL: Could not open score file.");
				System.out.println("FATAL: Unrecoverable exception, exiting.");
			}
		}
	}

	private ArrayList<Integer> _getWins(){
		ArrayList<Integer> score = new ArrayList<Integer>();
		score.add(0); score.add(0);
		this._checkFile();
		try{
			Scanner fr = new Scanner(new File(Dice.SCORE_FILE));
			for (int i = 0; i < 2; i++){
				score.set(i, fr.nextInt());
			}
		} catch (FileNotFoundException e){
			System.out.println("FATAL: Could not open score file.");
			System.out.println("FATAL: Unrecoverable exception, exiting.");
			System.exit(1);
		}
		return score;
	}

	private void _addWin(){
		ArrayList<Integer> score = this._getWins();
		try{
			PrintWriter w = new PrintWriter(Dice.SCORE_FILE);
			w.println(score.get(0) + 1);
			w.println(score.get(1));
			w.close();
		} catch (FileNotFoundException e){
			System.out.println("FATAL: Could not open score file.");
			System.out.println("FATAL: Unrecoverable exception, exiting.");
			System.exit(1);
		}
	}

	private void _addLoss(){
		ArrayList<Integer> score = this._getWins();
		try{
			PrintWriter w = new PrintWriter(Dice.SCORE_FILE);
			w.println(score.get(0));
			w.println(score.get(1) + 1);
			w.close();
		} catch (FileNotFoundException e){
			System.out.println("FATAL: Could not open score file.");
			System.out.println("FATAL: Unrecoverable exception, exiting.");
			System.exit(1);
		}
	}

	public ArrayList<ArrayList<Die>> getCurrentDices(){ return this.currentDices; }
}
