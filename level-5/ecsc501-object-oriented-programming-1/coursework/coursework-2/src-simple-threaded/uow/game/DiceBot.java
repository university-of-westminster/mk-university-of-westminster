package uow.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import uow.game.Dice;

public class DiceBot{
	private Dice dice;
	private int roll;
	private int state;
	private int filterState;
	private ArrayList<Integer> filters;
	private Random rng;

	/*
	 * REROLL ALGORITHM SELECTION
	 *
	 * PROBABILITY:
	 *   Available algorithms:
	 *     -1: Reroll everything below 4, keep otherwise
	 *      1: Reroll anything below 4, keep 6. Reroll 4 and 5 with okay-ish probability
	 *      2: Reroll 1, 2, 3 and 4 with very optimal probability, keep 5 and 6
	 *
	 *   WARNING: Read the required constraints of each algorithm in the comment above _probabilityCalc()
	 *            [constraints: ALGO_LOW_CUTOFF and ALGO_HIGH_CUTOFF constants]
	 *            Failure to meet specified algorithm constraints results in undefined behaviour.
	 *
	 * FILTERING:
	 *   Available algorithms:
	 *     -1: No filtering
	 *      1: Ensure a reroll doesn't bring you below win point (if already above)
	 */

	public static final int ALGO_FILTER      = -1;
	public static final int ALGO_PROBABILITY =  2;
	public static final int ALGO_PROBABILITY_LOW_CUTOFF  = 1;
	public static final int ALGO_PROBABILITY_HIGH_CUTOFF = 5;
	public static final double ALGO_PROBABILITY_ALWAYS   =  2.0;
	public static final double ALGO_PROBABILITY_NEVER    = -1.0;
	public static final double ALGO_PROBABILITY_UNKNOWN  = 999.0;
	public static final double ALGO_PROBABILITY_DEFAULT  = DiceBot.ALGO_PROBABILITY_ALWAYS;
	public static final double ALGO_FILTER_CHOICE_REMOVE = 100.0;
	public static final double ALGO_FILTER_CHOICE_KEEP   = DiceBot.ALGO_PROBABILITY_NEVER;
	public static final double ALGO_FILTER_CHOICE_REROLL = DiceBot.ALGO_PROBABILITY_ALWAYS;
	public static final double ALGO_FILTER_CHOICE_SKIP   = DiceBot.ALGO_PROBABILITY_UNKNOWN;
	public static final int ALGO_FILTER_STATE_RUNNING    = 1;
	public static final int ALGO_FILTER_STATE_FINISHED   = 2;

	public DiceBot(Dice d){
		this.dice = d;
		this.roll = 1;
		this.state = Dice.STATE_ROLL_ONE;
		this.rng = new Random();

		/*
		 * FILTER QUEUE
		 *
		 * Filters added below are ran sequentially in the specified order
		 */
		this.filters = new ArrayList<Integer>();
		this.filters.add(1);
	}

	public void play(){
		switch(this.state){
			case Dice.STATE_ROLL_ONE:
				this.dice.rerollRaw(Dice.ID_BOT);
				this.state++;
				break;
			case Dice.STATE_ROLL_TWO:
			case Dice.STATE_ROLL_THREE:
				this._play();
		}
	}

	public void finish(){
		while ((this.state != Dice.STATE_ROLL_FINISHED) && (this.state != Dice.STATE_ROLL_ONE)){
			this._play();
		}
		this.state = Dice.STATE_ROLL_ONE;
	}

	public void reset(){
		this.state = Dice.STATE_ROLL_ONE;
	}


	private void _play(){
		ArrayList<RerollProbabilityPair> probabilities = this._constructProbabilityList();
		ArrayList<Boolean> kept = new ArrayList<Boolean>();
		for (int i = 0; i < Dice.DICES; i++){ kept.add(true); }
		int totalRerolled = 0;
		for (int i = 0; i < probabilities.size(); i++){
			if (rng.nextDouble() <= probabilities.get(i).probability){
				kept.set(probabilities.get(i).index, false);
				totalRerolled++;
				System.out.println("Rerolling index [" + probabilities.get(i).index + "] - probability [" + probabilities.get(i).probability +"]");
			}
		}
		if (totalRerolled > 0){
			this.dice.rerollRaw(Dice.ID_BOT, kept);
			this.state = ((this.state == Dice.STATE_ROLL_TWO)?(Dice.STATE_ROLL_THREE):(Dice.STATE_ROLL_FINISHED));
			System.out.println("DEBUG: Bot rerolled " + totalRerolled + " dices");
		}
		else{
			this.state = Dice.STATE_ROLL_FINISHED;
			System.out.println("DEBUG: Bot finished this turn and will not reroll again this round.");
		}
	
		//float modifier = (this.state == Dice.STATE_ROLL_TWO)?(1.0):(0.5);
	}


	private ArrayList<RerollProbabilityPair> _constructProbabilityList(){
		ArrayList<RerollProbabilityPair> ret = new ArrayList<RerollProbabilityPair>();
		for (int i = 0; i < Dice.DICES; i++){
			ret.add(new RerollProbabilityPair(
				i,
				DiceBot.ALGO_PROBABILITY_UNKNOWN
			));
		}

		for (int i =0; i < this.filters.size(); i++){
			this.filterState = DiceBot.ALGO_FILTER_STATE_RUNNING;
			int index = 0;
			while (index < ret.size()){
				if (this.filterState == DiceBot.ALGO_FILTER_STATE_RUNNING){
					double result = this._filter(this.filters.get(i).intValue(), ret.get(index).index);
					if (result == DiceBot.ALGO_FILTER_CHOICE_REMOVE){
						ret.remove(index);
						index = -1;
					} else if (result == DiceBot.ALGO_FILTER_CHOICE_SKIP){
						// Do nothing
					}else{
						ret.get(i).probability = result;
					}
				}
				index++;
			}
		}

		// Set untouched probabilities 
		this._setDiceProbabilities(ret);

		Collections.sort(ret);
		this._limitReroll(ret);
		
		return ret;
	}

	private void _setDiceProbabilities(ArrayList<RerollProbabilityPair> p){
		for (int i = 0; i < p.size(); i++){
			if (p.get(i).probability == DiceBot.ALGO_PROBABILITY_UNKNOWN){
				p.get(i).probability = this._probabilityCalc(
					this.dice.getCurrentDices().get(Dice.ID_BOT).get(i).getValue()
				);
			}
		}
	}

	private void _limitReroll(ArrayList<RerollProbabilityPair> p){
		// Make sure not to cheat
		while (p.size() > Dice.MAX_REROLL_DICES){ p.remove(p.size()-1); }
	}



	/*
	 * Filtering algorithms
	 */
	private double _filter(int algo, int index){
		int currentRoll = 0;
		int totalCurrent = 0;
		switch(algo){
			case -1:
				return DiceBot.ALGO_FILTER_CHOICE_SKIP;
			case  1:
				totalCurrent = this.dice.score.get(Dice.ID_BOT).intValue();
				for (int i = 0; i < Dice.DICES; i++){
					currentRoll += this.dice.getCurrentDices().get(Dice.ID_BOT).get(index).getValue();
				}
				totalCurrent += currentRoll;

				if (totalCurrent < this.dice.winThreshold){
					return DiceBot.ALGO_FILTER_CHOICE_SKIP;
				}

				if (
					(totalCurrent - this.dice.getCurrentDices().get(Dice.ID_BOT).get(index).getValue())
					<
					this.dice.winThreshold
				){
					return DiceBot.ALGO_FILTER_CHOICE_REMOVE;
				}
				return DiceBot.ALGO_FILTER_CHOICE_SKIP;
			case  2:
				for (int i = 0; i < Dice.DICES; i++){
					currentRoll += this.dice.getCurrentDices().get(Dice.ID_BOT).get(index).getValue();
				}
				
				if (currentRoll >= 24){
					if (this.dice.getCurrentDices().get(Dice.ID_BOT).get(index).getValue() < 6){
						this.filterState = DiceBot.ALGO_FILTER_STATE_FINISHED;
						return DiceBot.ALGO_FILTER_CHOICE_REROLL;
					}
				} else if (currentRoll >= 20){
					if (this.dice.getCurrentDices().get(Dice.ID_BOT).get(index).getValue() < 5){
						this.filterState = DiceBot.ALGO_FILTER_STATE_FINISHED;
						return DiceBot.ALGO_FILTER_CHOICE_REROLL;
					}
				}
				return DiceBot.ALGO_FILTER_CHOICE_SKIP;
		}
		return DiceBot.ALGO_FILTER_CHOICE_SKIP;
	}

	/*
	 * Reroll probability algorithms
	 *
	 * Probability algorithm constraints (inclusive):
	 *  -1:
	 *      MIN: 1
	 *      MAX: 6
	 *   1:
	 *      MIN: 1
	 *      MAX: 6
	 *   2:
	 *      MIN: 2
	 *      MAX: 4
	 *
	 *   When using these algorithms _DO NOT_ exceed the given constraints in ALGO_{HIGH,LOW}_CUTOFF!
	 */
	private double _probabilityCalc(int x){
		if (x >= DiceBot.ALGO_PROBABILITY_HIGH_CUTOFF){ return DiceBot.ALGO_PROBABILITY_NEVER; }
		if (x <= DiceBot.ALGO_PROBABILITY_LOW_CUTOFF){ return DiceBot.ALGO_PROBABILITY_ALWAYS; }

		switch (DiceBot.ALGO_PROBABILITY){
			case -1:
				if (x > 3){ return DiceBot.ALGO_PROBABILITY_NEVER;}
				else{ return DiceBot.ALGO_PROBABILITY_ALWAYS; }
			case  1:
				return (
					  (-0.00729167 * Math.pow(x, 4))
					+ (0.127546 * Math.pow(x, 3))
					- (0.735069 * Math.pow(x, 2))
					+ (1.35152 * x)
					+ (0.258333)
				);
			case  2:
				return (
					  (0.584434 * Math.sin(x))
					- (0.520957 * Math.cos(x))
					+ (0.201781)
				);
		}
		return DiceBot.ALGO_PROBABILITY_DEFAULT;
	}
}


class RerollProbabilityPair implements Comparable<RerollProbabilityPair>{
	public int index;
	public double probability;
	public RerollProbabilityPair(int i, double p){
		this.index = i;
		this.probability = p;
	}

	public int compareTo(RerollProbabilityPair other){
		return Double.compare(other.probability, this.probability);
	}
}
