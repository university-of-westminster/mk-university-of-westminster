package uow.thread;

import javax.swing.SwingWorker;

import uow.game.Dice;

public abstract class DiceWorker extends SwingWorker<Void, Object>{
	protected Dice dice;

	public DiceWorker(Dice d){
		super();
		this.dice = d;
	}

	public abstract Void doInBackground();
}
