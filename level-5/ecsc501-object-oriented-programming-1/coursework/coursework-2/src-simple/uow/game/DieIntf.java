package uow.game;

import javax.swing.ImageIcon;

public interface DieIntf{
	public ImageIcon getDieImage();
	public void setImage(String path);
	public void setImage(ImageIcon image);
	public void setValue(int value);
	public int  getValue();
}
