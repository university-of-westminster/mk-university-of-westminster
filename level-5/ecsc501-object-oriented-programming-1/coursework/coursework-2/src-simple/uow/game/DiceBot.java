package uow.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import uow.game.Dice;

public class DiceBot{
	private Dice dice;
	private int roll;
	private int state;
	private Random rng;

	public DiceBot(Dice d){
		this.dice = d;
		this.roll = 1;
		this.state = Dice.STATE_ROLL_ONE;
		this.rng = new Random();
	}

	public void play(){
		switch(this.state){
			case Dice.STATE_ROLL_ONE:
				this.dice.reroll(Dice.ID_BOT);
				this.state++;
				break;
			case Dice.STATE_ROLL_TWO:
			case Dice.STATE_ROLL_THREE:
				this._play();
		}
	}

	public void finish(){
		while (this.state != Dice.STATE_ROLL_FINISHED){
			this._play();
		}
		this.dice.scoreCurrentRoll(Dice.ID_BOT);
		this.state = Dice.STATE_ROLL_ONE;
	}

	public void reset(){
		this.state = Dice.STATE_ROLL_ONE;
	}


	private void _play(){
		ArrayList<RerollProbabilityPair> probabilities = this._getDiceProbabilities();
		ArrayList<Boolean> kept = new ArrayList<Boolean>();
		for (int i = 0; i < Dice.DICES; i++){ kept.add(true); }
		int totalRerolled = 0;
		for (int i = 0; i < probabilities.size(); i++){
			if (rng.nextDouble() <= probabilities.get(i).probability){
				kept.set(probabilities.get(i).index, false);
				totalRerolled++;
				System.out.println("Rerolling index [" + probabilities.get(i).index + "] - probability [" + probabilities.get(i).probability +"]");
			}
		}
		if (totalRerolled > 0){
			this.dice.reroll(Dice.ID_BOT, kept);
			this.state = ((this.state == Dice.STATE_ROLL_TWO)?(Dice.STATE_ROLL_THREE):(Dice.STATE_ROLL_FINISHED));
			System.out.println("DEBUG: Bot rerolling " + totalRerolled);
		}
		else{
			this.state = Dice.STATE_ROLL_FINISHED;
			System.out.println("DEBUG: Bot finished this turn.");
		}
	
		/*
		float modifier = (this.state == Dice.STATE_ROLL_TWO)?(1.0):(0.5);
		ArrayList<RerollProbabilityPair> probability = this._getDiceProbabilities();
		*/
	}


	private ArrayList<RerollProbabilityPair> _getDiceProbabilities(){
		ArrayList<RerollProbabilityPair> ret = new ArrayList<RerollProbabilityPair>();
		for (int i = 0; i < 5; i++){
			ret.add(
				new RerollProbabilityPair(
					i,
					this._probabilityCalc(
						this.dice.getCurrentDices().get(Dice.ID_BOT).get(i).getValue()
					)
				)
			);
		}
		Collections.sort(ret);
		return this._limitReroll(ret);
	}

	private ArrayList<RerollProbabilityPair> _limitReroll(ArrayList<RerollProbabilityPair> p){
		while (p.size() > Dice.MAX_REROLL_DICES){ p.remove(p.size()-1); }
		return p;
	}

	private int _rangeContain(int x){
		if (x < Dice.MAX_REROLL_DICES){
			return x;
		}
		return Dice.MAX_REROLL_DICES;
	}

	/* Reroll probability math magic */
	private double _probabilityCalc(int x){
		if (x > 5){ return -1.0; }
		if (x < 4){ return 1.0; }
		return (
			  (-0.00729167 * Math.pow(x, 4))
			+ (0.127546 * Math.pow(x, 3))
			- (0.735069 * Math.pow(x, 2))
			+ (1.35152 * x)
			+ (0.258333)
		);
	}
}


class RerollProbabilityPair implements Comparable<RerollProbabilityPair>{
	public int index;
	public double probability;
	public RerollProbabilityPair(int i, double p){
		this.index = i;
		this.probability = p;
	}

	public int compareTo(RerollProbabilityPair other){
		return Double.compare(other.probability, this.probability);
	}
}
