package uow.gui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import uow.game.Dice;

public abstract class DiceActionListener implements ActionListener{
	protected Dice dice;
	public DiceActionListener(Dice d){ this.dice = d; }
	public abstract void actionPerformed(ActionEvent e);
}
