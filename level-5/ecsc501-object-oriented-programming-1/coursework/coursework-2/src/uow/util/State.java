package uow.util;

import ArrayList;

import uow.exceptions.StateMachineInvalidStepException;

public class State{
	private String name;
	private Runnable callback;
	private ArrayList<State> nextStep;

	public State(String name){
		this.nextStep = new ArrayList<String>();
	}

	public State(String nextStep, Runnable callback){
		this.nextStep = new ArrayList<String>();
		this.nextStep.add(nextStep);
		this.callback = callback;
	}

	public State(ArrayList<String> nextStep, Runnable callback){
		this.nextStep = nextStep;
		this.callback = callback;
	}

	public String next() throws StateMachineInvalidStepException{
		if (this.nextStep.size() != 1){
			throw new StateMachineInvalidStepException();
		}
		else{ return this.nextStep.get(0); }
	}

	public String next(String name) throws StateMachineInvalidStepException{
		if (this.nextStep.size() < 1){ throw new StateMachineInvalidStepException(); }
		for (item : this.nextStep){
			if (item.toLowerCase().equals(name.toLowerCase)){
				return name;
			}
		}
		throw new StateMachineInvalidStepException();
	}

	public boolean isFinal(){
		return (this.nextStep.size() == 0);
	}

	public String getName(){ return this.name; }
}
