package uow.util;

import uow.game.Dice;

public abstract class DiceRunnable implements Runnable{
	private Dice dice;
	public DiceRunnable(Dice d){ this.dice = d; }
	public abstract void run();
}
