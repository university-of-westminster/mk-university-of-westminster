package uow.game;

import uow.gui.GraphicalInterface;
import uow.game.Dice;

public class Game{
	private final int PLAYER   = 0;
	private final int COMPUTER = 1;
	private int[] score = [0, 0];
	private Dice dice;
	private GraphicalInterface gui;

	public Game(){
		this.dice = new Dice();
		this.gui  = new GraphicalInterface("Dice Game", true);
		this._initHooks();
	}


	// TODO


	private void _initHooks(){
		this.gui.hookThrow(new DiceActionListener(this.dice, this.gui){
			@Override
			public void actionPerformed(ActionEvent e){
				ArrayList<Die> left = this.dice.roll();
				ArrayList<Die> right = this.dice.roll();
				for (int i = 0; i < 5; i++){
					this.gui.setImage(this.gui.IMAGES_LEFT, i, left.get(i).getImage());
					this.gui.setImage(this.gui.IMAGES_RIGHT, i, right.get(i).getImage());
				}
			}
		});

		this.gui.hookScore(new DiceActionListener(this.dice, this.gui){
			@Override
			public void actionPerformed(ActionEvent e){
				
			}
		});
	}
}
