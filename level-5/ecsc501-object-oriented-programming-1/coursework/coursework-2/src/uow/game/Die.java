import uow.game.DieIntf;

import javax.swing.ImageIcon;

public class Die implements DieIntf, Comparable<Die>{
	private ImageIcon image;
	private int value;

	public Die(){
		this.image = new ImageIcon("img/none.jpg");
		this.value = -1;
	}

	public Die(int value){
		this.image = new ImageIcon("img/die-"+value+".jpg");
		this.value = value;
	}


	public void setImage(String path){ this.image = new ImageIcon(path); }
	public void setValue(int value){ this.value = value; }

	public ImageIcon getDieImage(){ return this.image; }
	public int getValue(){ return this.value; }

	/* Comparable impl. */
	public int compareTo(Die other){
		return Integer.compare(other.getValue(), this.getValue());
	}
}
