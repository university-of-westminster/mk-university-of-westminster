import uow.game.Die;
import java.util.ArrayList;
import java.util.Random;


public class Dice{
	ArrayList<Die> dies;
	Random rng;

	public Dice(){
		this.dies = new ArrayList<Die>();
		for (int i = 0; i < 6; i++){
			this.dies.add(new Die(i+1);
		}
		this.rng = new Random();
	}

	public ArrayList<Die> roll(){
		ArrayList<Die> ret = new ArrayList<Die>();
		for (int i = 0; i < 5; i++){
			ret.add(this.dies.get(this.rng.nextInt(6)));
		}
		return ret;
	}
}
