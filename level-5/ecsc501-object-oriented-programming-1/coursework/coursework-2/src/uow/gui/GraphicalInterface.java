package uow.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

import java.util.ArrayList;

import uow.gui.DiceActionListener;

public class GraphicalInterface{
	private JFrame window;
	private JPanel left;
	private JPanel right;
	private JPanel bottom;
	private ArrayList<JButton> imagesLeft;
	private ArrayList<JButton> imagesRight;
	private ArrayList<JCheckBox> cbsLeft;
	private ArrayList<JCheckBox> cbsRight;
	private JButton btnThrow;
	private JButton btnScore;
	private JButton btnReset;

	private final String iconPath = "img/";
	private ImageIcon iconNone;

	public final int IMAGES_LEFT  = 1;
	public final int IMAGES_RIGHT = 2;

	public GraphicalInterface(String title, boolean limitResizing){
		this._initResources(); // Create non-component objects
		this._initComponents(title); // Create window components
		this._initStyling(); // Initialize component styling and layouts
		if (limitResizing){ this._initResizingLimit(); } // Limit main window resizing?
		this._containerize(); // Insert components into their relevant container

		this.window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.window.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				System.out.println("Exiting... [def.WindowAdapter]");
				System.exit(0);
			}
		});
	}


	public void display(){
		this.window.pack();
		this.window.setLocation(
			Toolkit.getDefaultToolkit().getScreenSize().width/2-this.window.getSize().width/2,
			Toolkit.getDefaultToolkit().getScreenSize().height/2-this.window.getSize().height/2
		);
		this.window.setVisible(true);
	}

	public void hide(){
		this.window.setVisible(false);
	}


	public enableControls(boolean bThrow, bScore, bReset, cReroll){
		this.btnThrow.setEnabled(bThrow);
		this.btnScore.setEnabled(bScore);
		this.btnReset.setEnabled(bReset);
		for (int i = 0; i < 5; i++){
			this.cbsLeft.get(i).setEnabled(cReroll);
			this.cbsRight.get(i).setEnabled(cReroll);
		}
	}

	public void setImage(int imageId, int index, ImageIcon image){
		try{
			switch (imageId) {
				case this.IMAGE_LEFT:
					this.imagesLeft.get(index).setIcon(image);
					break;
				case this.IMAGES_RIGHT:
					this.imagesRight.get(index).setIcon(image);
					break;
			}
		} catch (OutOfBoundsException e){}
	}


	public void hookThrow(DiceActionListener l){
		this.btnThrow.addActionListener(l);
	}

	public void hookScore(DiceActionListener l){
		this.btnScore.addActionListener(l);
	}

	public void hookReset(DiceActionListener l){
		this.btnReset.addActionListener(l);
	}


	private void _initResources(){
		this.imagesLeft = new ArrayList<JButton>();
		this.imagesRight = new ArrayList<JButton>();
		this.cbsLeft = new ArrayList<JCheckBox>();
		this.cbsRight = new ArrayList<JCheckBox>();
		this.iconNone = new ImageIcon(this.iconPath + "none.png");
	}

	private void _initComponents(String title){
		this.window = new JFrame(title);
		this.left = new JPanel();
		this.right = new JPanel();
		this.bottom = new JPanel();
		this.btnThrow = new JButton("Throw");
		this.btnScore = new JButton("Score");
		this.btnReset = new JButton("Restart Game");

		for (int i =0; i < 5; i++){
			this.imagesLeft.add(new JButton(this.iconNone));
			this.imagesRight.add(new JButton(this.iconNone));

			this.cbsLeft.add(new JCheckBox("reroll"));
			this.cbsRight.add(new JCheckBox("reroll"));
		}
	}

	private void _initStyling(){
		/* Initialize layouts */
		this.window.getContentPane().setLayout(new BorderLayout());
		this.left.setLayout(new GridLayout(0,2));
		this.right.setLayout(new GridLayout(0,2));
		for (int i = 0; i < 5; i++){
			JButton tmp = this.imagesLeft.get(i);
			tmp.setBorderPainted(false);
			tmp.setFocusPainted(false);
			tmp.setContentAreaFilled(false);
			this.cbsLeft.get(i).setHorizontalTextPosition(SwingConstants.LEFT);
			this.cbsLeft.get(i).setHorizontalAlignment(SwingConstants.RIGHT);

			tmp = this.imagesRight.get(i);
			tmp.setBorderPainted(false);
			tmp.setFocusPainted(false);
			tmp.setContentAreaFilled(false);
		}
	}

	private void _initResizingLimit(){
		this.window.setMinimumSize(new Dimension(525, 560));
		this.window.setPreferredSize(new Dimension(725, 600));
	}

	private void _containerize(){
		for (int i = 0; i < 5; i++){
			this.left.add(this.cbsLeft.get(i));
			this.left.add(this.imagesLeft.get(i));
			this.right.add(this.imagesRight.get(i));
			this.right.add(this.cbsRight.get(i));
		}

		this.bottom.add(this.btnThrow);
		this.bottom.add(this.btnScore);
		this.bottom.add(this.btnReset);

		this.window.add(this.left, "West");
		this.window.add(this.right, "East");
		this.window.add(this.bottom, "South");
	}
}
