import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import uow.gui.GraphicalInterface;
import uow.game.Dice;

public abstract class DiceActionLitener implements ActionListener{
	private Dice dice;
	private GraphicalInterface gui;
	public DiceActionListener(Dice d){ this.dice = d; }
	public DiceActionListener(GraphicalInterface g){ this.gui = g; }
	public DiceActionListener(Dice d, GraphicalInterface g){ this.dice = d; this.gui = g; }
	abstract void actionPerformed(ActionEvent e);
}
