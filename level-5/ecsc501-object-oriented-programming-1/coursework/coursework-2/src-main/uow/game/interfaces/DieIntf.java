package uow.game.interfaces;

import javax.swing.ImageIcon;

public interface DieIntf{
	public ImageIcon getDieImage();
	public int  getValue();

	public void setImage();
	public void setImage(String path);
	public void setImage(ImageIcon image);
	public void setValue(int value);
}
