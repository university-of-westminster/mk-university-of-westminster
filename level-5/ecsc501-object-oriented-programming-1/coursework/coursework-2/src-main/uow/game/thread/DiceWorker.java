package uow.game.thread;

import javax.swing.SwingWorker;

import uow.game.DiceGame;

public abstract class DiceWorker extends SwingWorker<Void, Object>{
	protected DiceGame game;

	public DiceWorker(DiceGame g){
		super();
		this.game = g;
	}

	public abstract Void doInBackground();
}
