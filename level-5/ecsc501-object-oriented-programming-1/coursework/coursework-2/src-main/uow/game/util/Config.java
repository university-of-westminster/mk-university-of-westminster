package uow.game.util;

public class Config{
	// Global settings
	public static final int GLOBAL_ID_PLAYER = 0;
	public static final int GLOBAL_ID_BOT    = 1;

	public static final String GLOBAL_POINTS_SEPARATOR = " | ";
	public static final String GLOBAL_SCORE_SEPARATOR  = " / ";

	// Game settings
	public static final int GAME_TOTAL_DICES           = 5;
	public static final int GAME_MAX_REROLLS           = Config.GAME_TOTAL_DICES;
	public static final int GAME_DEFAULT_WIN_THRESHOLD = 101;

	// GUI settings
	public static final boolean GUI_RESTRICT_RESIZING    = false;
	public static final String  GUI_DEFAULT_NOTIFICATION = "<no notification>";

	// Paths
	public static final String PATH_ICONS = "img/";
	public static final String PATH_SCORE = "score.dat";
	public static final String PATH_DEBUG = "debug.log";

	// Other
	public static final boolean DEBUG       = false;
	public static final boolean DEBUG_PRINT = true;
	public static final boolean DEBUG_FILE  = false;
	
	public static final boolean DEBUG_RIGGED = false;
}
