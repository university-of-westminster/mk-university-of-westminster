package uow.game;

import uow.game.interfaces.DieIntf;
import uow.game.util.Config;

import javax.swing.ImageIcon;

public class Die implements DieIntf, Comparable<Die>{
	private ImageIcon image;
	private int value;

	public Die(){
		this.value = -1;
		this.setImage();
	}

	public Die(int value){
		this.value = value;
		this.setImage();
	}


	public void setImage(){ this.image = new ImageIcon(Config.PATH_ICONS + ((this.value <= 0)?("none.png"):("dice-"+value+".png"))); }
	public void setImage(String path){ this.image = new ImageIcon(path); }
	public void setImage(ImageIcon i){ this.image = i; }
	public void setValue(int value){
		this.value = ((value < 1) || (value > 6))?(-1):(value);
		this.setImage();
	}

	public ImageIcon getDieImage(){ return this.image; }
	public int getValue(){ return this.value; }

	/* Comparable impl. */
	public int compareTo(Die other){
		return Integer.compare(other.getValue(), this.getValue());
	}
}
