package uow.game.gui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import uow.game.DiceGame;

public abstract class DiceActionListener implements ActionListener{
	protected DiceGame game;
	public DiceActionListener(DiceGame g){ this.game = g; }
	public abstract void actionPerformed(ActionEvent e);
}
