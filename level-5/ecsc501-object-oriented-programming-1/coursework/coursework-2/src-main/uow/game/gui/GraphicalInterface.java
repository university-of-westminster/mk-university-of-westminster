package uow.game.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.WindowConstants;

import java.awt.Toolkit;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.util.ArrayList;

import uow.game.util.Config;
import uow.game.gui.DiceActionListener;

public class GraphicalInterface{
	public static final int ID_PLAYER = Config.GLOBAL_ID_PLAYER;
	public static final int ID_BOT = Config.GLOBAL_ID_BOT;
	
	private JFrame window;
	private JPanel top;
	private JPanel left;
	private JPanel right;
	private JPanel bottom;
	private ArrayList< ArrayList<JButton> > images;
	private ArrayList<JCheckBox> cbKept;
	private JButton btnThrow;
	private JButton btnScore;
	private JButton btnReset;
	private JLabel lblPoints;
	private JLabel lblScore;
	private JLabel lblInterim;
	private JLabel lblNotification;

	private ImageIcon iconNone;
	
	public GraphicalInterface(){
		this.iconNone = new ImageIcon(Config.PATH_ICONS + "none.png");
		this._initGUI();
	}
	
	
	public void notify(String message){ this.lblNotification.setText(message); }
	public void setScore(int wins, int losses){ this.lblScore.setText(wins + Config.GLOBAL_POINTS_SEPARATOR + losses); }
	public void setPoints(int bot, int player){ this.lblPoints.setText(bot + Config.GLOBAL_POINTS_SEPARATOR + player); }
	public void setInterim(int bot, int player){ this.lblInterim.setText(bot + Config.GLOBAL_POINTS_SEPARATOR + player); }

	public void updateDice(int playerId, int index, ImageIcon image){
		this.images.get(playerId).get(index).setIcon(image);
	}

	public void enableThrow(boolean e){ this.btnThrow.setEnabled(e); }
	public void enableScore(boolean e){ this.btnScore.setEnabled(e); }
	public void enableReset(boolean e){ this.btnReset.setEnabled(e); }
	public void enableControls(boolean t, boolean s, boolean r, boolean c){
		this.btnThrow.setEnabled(t);
		this.btnScore.setEnabled(s);
		this.btnReset.setEnabled(r);
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			this.cbKept.get(i).setEnabled(c);
		}
	}


	public ArrayList<Boolean> getSelected(){
		ArrayList<Boolean> ret = new ArrayList<Boolean>();
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			ret.add(this.cbKept.get(i).isSelected());
		}
		return ret;
	}


	public void resetDisplay(boolean resetDiceDisplay){
		if (resetDiceDisplay){
			for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
				this.images.get(GraphicalInterface.ID_PLAYER).get(i).setIcon(this.iconNone);
				this.images.get(GraphicalInterface.ID_BOT).get(i).setIcon(this.iconNone);
			}
			this.lblPoints.setText("0" + Config.GLOBAL_POINTS_SEPARATOR + "0");
		}
		this.lblInterim.setText("0" + Config.GLOBAL_POINTS_SEPARATOR + "0");
		this.enableControls(true, false, true, false);
		this.resetCheckboxes();
	}

	private void resetCheckboxes(){
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			this.cbKept.get(i).setSelected(false);
		}
	}

	public void checkboxHook(){
		int total = 0;
		ArrayList<Boolean> selected = this.getSelected();
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			if (selected.get(i)){ total++; }
		}
		if (total >= Config.GAME_TOTAL_DICES){
			this.enableThrow(false);
		} else{
			this.enableThrow(((Config.GAME_TOTAL_DICES - total) <= Config.GAME_MAX_REROLLS)?(true):(false));
		}
	}

	public void hookThrow(DiceActionListener l){
		this.btnThrow.addActionListener(l);
	}
	public void hookScore(DiceActionListener l){
		this.btnScore.addActionListener(l);
	}
	public void hookReset(DiceActionListener l){
		this.btnReset.addActionListener(l);
	}
	public void hookCheckboxes(DiceActionListener l){
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			this.cbKept.get(i).addActionListener(l);
		}
	}


	/* === INIT FUNCTIONS === */
	private void _initGUI(){
		this._initComponents();
		this._initStyling();
		this._initContainerize();
		this._initFinish();
	}
	
	private void _initComponents(){
		// Initialize resources
		this.images = new ArrayList< ArrayList<JButton> >();
		this.images.add(new ArrayList<JButton>());
		this.images.add(new ArrayList<JButton>());

		this.cbKept = new ArrayList<JCheckBox>();

		// Initialize components
		this.window = new JFrame("Dice Game");

		this.top = new JPanel();
		this.left = new JPanel();
		this.right = new JPanel();
		this.bottom = new JPanel();

		this.btnThrow = new JButton("Throw");
		this.btnScore = new JButton("Score");
		this.btnReset = new JButton("Restart Game");

		this.lblPoints = new JLabel("0" + Config.GLOBAL_POINTS_SEPARATOR + "0");
		this.lblScore = new JLabel("0" + Config.GLOBAL_SCORE_SEPARATOR + "0");
		this.lblInterim = new JLabel("0" + Config.GLOBAL_POINTS_SEPARATOR + "0");
		this.lblNotification = new JLabel(Config.GUI_DEFAULT_NOTIFICATION);

		// Initialize displayed images and checkboxes
		for (int i =0; i < Config.GAME_TOTAL_DICES; i++){
			this.images.get(GraphicalInterface.ID_PLAYER).add(new JButton(this.iconNone));
			this.images.get(GraphicalInterface.ID_BOT).add(new JButton(this.iconNone));
	
			this.cbKept.add(new JCheckBox("keep"));
		}
	}
	
	private void _initStyling(){
		// Setup styles & layouts
		this.window.getContentPane().setLayout(new BorderLayout());
		this.top.setLayout(new GridLayout(0, 3));
		this.left.setLayout(new GridLayout(0, 2));
		this.right.setLayout(new GridLayout(0, 2));
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			JButton tmp = this.images.get(GraphicalInterface.ID_BOT).get(i);
			tmp.setBorderPainted(false);
			tmp.setFocusPainted(false);
			tmp.setContentAreaFilled(false);

			tmp = this.images.get(GraphicalInterface.ID_PLAYER).get(i);
			tmp.setBorderPainted(false);
			tmp.setFocusPainted(false);
			tmp.setContentAreaFilled(false);
		}
		this.lblPoints.setHorizontalAlignment(JLabel.CENTER);
		this.lblScore.setHorizontalAlignment(JLabel.CENTER);
		this.lblInterim.setHorizontalAlignment(JLabel.CENTER);
		this.lblNotification.setHorizontalAlignment(JLabel.CENTER);
	}

	private void _initContainerize(){
		// Put everything in place
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			JLabel tmp = new JLabel("Dice " + (i+1));
			tmp.setHorizontalAlignment(JLabel.CENTER);
			this.left.add(tmp);
			this.left.add(this.images.get(GraphicalInterface.ID_BOT).get(i));
			this.right.add(this.images.get(GraphicalInterface.ID_PLAYER).get(i));
			this.right.add(this.cbKept.get(i));
		}
		JLabel tmp = new JLabel("POINTS");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		tmp = new JLabel("WINS" + Config.GLOBAL_SCORE_SEPARATOR + "LOSSES");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		tmp = new JLabel("INTERIM");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		this.top.add(this.lblPoints);
		this.top.add(this.lblScore);
		this.top.add(this.lblInterim);
		tmp = new JLabel("COMPUTER");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		this.top.add(this.lblNotification);
		tmp = new JLabel("PLAYER");
		tmp.setHorizontalAlignment(JLabel.CENTER);
		this.top.add(tmp);
		this.bottom.add(this.btnThrow);
		this.bottom.add(this.btnScore);
		this.bottom.add(this.btnReset);
		this.window.add(this.top, "North");
		this.window.add(this.left, "West");
		this.window.add(this.right, "East");
		this.window.add(this.bottom, "South");
	}
	
	private void _initFinish(){
		// Final init - set window size, restrict resizing, install default winAdapter, pack, position & display
		if (Config.GUI_RESTRICT_RESIZING){
			this.window.setMinimumSize(new Dimension(450, 100 + (Config.GAME_TOTAL_DICES * 110)));
		}
		this.window.setPreferredSize(new Dimension(725, 600));
		this.window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.window.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				System.out.println("Exiting... [def.WA]");
				System.exit(0);
			}
		});
		this.window.pack();
		this.window.setLocation(
			Toolkit.getDefaultToolkit().getScreenSize().width/2-this.window.getSize().width/2,
			Toolkit.getDefaultToolkit().getScreenSize().height/2-this.window.getSize().height/2
		);
		this.window.setVisible(true);
	}
}
