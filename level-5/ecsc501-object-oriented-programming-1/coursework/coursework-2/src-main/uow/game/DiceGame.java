package uow.game;

import java.io.FileNotFoundException;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import java.util.ArrayList;

import uow.game.util.Config;
import uow.game.util.Logger;

import uow.game.gui.GraphicalInterface;
import uow.game.gui.DiceActionListener;

import uow.game.Die;

import uow.game.ai.DiceBot;

import uow.game.thread.DiceWorker;

public class DiceGame{
	public static final int ID_PLAYER = Config.GLOBAL_ID_PLAYER;
	public static final int ID_BOT    = Config.GLOBAL_ID_BOT;

	public static final int STATE_ROLL_ONE      = 1;
	public static final int STATE_ROLL_TWO      = 2;
	public static final int STATE_ROLL_THREE    = 3;
	public static final int STATE_ROLL_DRAW     = 4;
	public static final int STATE_ROLL_FINISHED = 5;
	public static final int STATE_END           = 9;

	private Dice dice;
	private DiceBot bot;
	private GraphicalInterface gui;

	private int state;

	private ArrayList<ArrayList<Die>> currentDices;
	public ArrayList<Integer> points;
	private int winThreshold;

	public DiceGame(){
		this.dice = new Dice();
		this.gui  = new GraphicalInterface();
		this.bot  = new DiceBot(this);

		this._initHooks();

		this.state = DiceGame.STATE_ROLL_ONE;

		this.points = new ArrayList<Integer>();
		this.points.add(0); this.points.add(0);

		this.currentDices = new ArrayList<ArrayList<Die>>();
		this.currentDices.add(new ArrayList<Die>());
		this.currentDices.add(new ArrayList<Die>());
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			this.currentDices.get(DiceGame.ID_PLAYER).add(new Die());
			this.currentDices.get(DiceGame.ID_BOT).add(new Die());
		}

		// Custom win threshold?
		this.winThreshold = Config.GAME_DEFAULT_WIN_THRESHOLD;
		Object tmp = JOptionPane.showInputDialog(
			null,
			"Winning score threshold:",
			"Initial configuration",
			JOptionPane.QUESTION_MESSAGE,
			null,
			null,
			Config.GAME_DEFAULT_WIN_THRESHOLD
		);
		if (tmp != null){
			try{
				this.winThreshold = Integer.parseInt(((String)tmp));
			} catch (NumberFormatException e){
				Logger.L("Invalid win threshold input, defaulting to " + Config.GAME_DEFAULT_WIN_THRESHOLD);
				this.winThreshold = Config.GAME_DEFAULT_WIN_THRESHOLD;
			}
		}

		this.gui.notify("Win threshold set to " + this.winThreshold);

		this._reset();
	}

	// Reroll selected dices, don't display
	public void reroll(int playerId, ArrayList<Boolean> kept){
		int rerolled = 0;
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			if (!kept.get(i)){
				this.currentDices.get(playerId).set(i, this.dice.roll(1).get(0));
				rerolled++;
			}
		}
		if (rerolled > Config.GAME_MAX_REROLLS){
			System.out.println("FATAL: Caught cheater.");
			System.out.println("FATAL: Player ID [" + playerId + "] has been found to be cheating.");
			System.out.println("FATAL: Game will now exit.");
			System.exit(1);
		}
	}

	// Reroll all dices
	public void reroll(int playerId){
		ArrayList<Boolean> allFalse = new ArrayList<Boolean>();
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){ allFalse.add(false); }
		this.reroll(playerId, allFalse);
	}


	public void executeThrow(){
		Logger.L("DEBUG: Throwing for state " + this.state);
		switch (this.state){
			case DiceGame.STATE_ROLL_THREE:
			case DiceGame.STATE_ROLL_TWO:
				/*
				 * THREADING
				 *
				 * We could make our two threads work together using process() or other means
				 * so we don't block the event thread by calling get() on the threads inside it.
				 * 
				 * The SwingWorker done() method is supposed to get executed synchronously on the event
				 * thread, however I am not certainly trusting that this is the case. If it is indeed
				 * true, it would be okay to allow both threads to execute GUI changes in their done()
				 * methods. We will instead wait for both threads to complete before updating the GUI.
				 *
				 * We can avoid both the thread itnteraction, and blocking event thread, by spawning a
				 * new thread in the event thread and create the player and bot threads from within
				 * this new thread, thus making our event thread fully responsive while being able to
				 * wait for both player and computer worker threads to complete.
				 *
				 * (The above stems from my understanding of threading in Swing, it might be wrong)
				 *
				 * The implementation I chose is very easy to do, while having basically the same
				 * results as the 'proper' way,  so that is how all threading is implemented
				 * in this application.
				 */
				// Spawn encapsulating thread
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;

						tPlayer = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.reroll(DiceGame.ID_PLAYER, this.game.gui.getSelected());
								return null;
							}
						};
						tBot = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.bot.play();
								return null;
							}
						};

						// Run both worker threads
						tPlayer.execute();
						tBot.execute();

						// Wait for both threads to complete
						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}

						this.game.setState((this.game.state == DiceGame.STATE_ROLL_TWO)?(DiceGame.STATE_ROLL_THREE):(DiceGame.STATE_ROLL_FINISHED));
						return null;
					}
					@Override
					public void done(){
						// Execute GUI updates after both worker threads completed
						this.game.gui.notify("Second reroll");
						this.game.displayUpdate();
						this.game.displayInterimScore();
						if (this.game.state == DiceGame.STATE_ROLL_FINISHED){ this.game.executeScore(); }
						else{ this.game.gui.enableControls(true, true, true, true); }
					}
				}).execute();
				break;
			case DiceGame.STATE_ROLL_ONE:
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;

						tPlayer = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.reroll(DiceGame.ID_PLAYER);
								return null;
							}
						};
						tBot = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.bot.play();
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}

						return null;
					}
					@Override
					public void done(){
						this.game.state = DiceGame.STATE_ROLL_TWO;
						this.game.gui.notify("First reroll");
						this.game.displayInterimScore();
						this.game.displayUpdate();
						this.game.gui.enableControls(true, true, true, true);
						this.game.checkboxHook();
						
					}
				}).execute();
				break;
			case DiceGame.STATE_ROLL_DRAW:
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;

						tPlayer = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.reroll(DiceGame.ID_PLAYER);
								return null;
							}
						};
						tBot = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.bot.play();
								this.game.bot.reset();
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}

						return null;
					}
					@Override
					public void done(){
						this.game.displayUpdate();
						this.game.displayInterimScore();
						this.game.executeScore();
					}
				}).execute();
		}
	}

	public void executeScore(){
		switch (this.state){
			case DiceGame.STATE_ROLL_DRAW:
			case DiceGame.STATE_ROLL_FINISHED:
			case DiceGame.STATE_ROLL_ONE:
			case DiceGame.STATE_ROLL_TWO:
			case DiceGame.STATE_ROLL_THREE:
				(new DiceWorker(this){
					@Override
					public Void doInBackground(){
						DiceWorker tPlayer;
						DiceWorker tBot;
						
						tPlayer = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.scoreCurrentRoll(DiceGame.ID_PLAYER);
								return null;
							}
						};
						tBot = new DiceWorker(this.game){
							@Override
							public Void doInBackground(){
								this.game.bot.finish();
								this.game.scoreCurrentRoll(DiceGame.ID_BOT);
								return null;
							}
						};

						tPlayer.execute();
						tBot.execute();

						try{
							tPlayer.get();
							tBot.get();
						} catch (Exception e){}
						
						return null;
					}
					@Override
					public void done(){
						this.game.displayUpdate();
						this.game.displayPoints();
						this.game.resetDisplay(false);
						this.game.checkWin();
						if (this.game.state == DiceGame.STATE_ROLL_DRAW){
							this.game.gui.enableControls(true, false, true, false);
							this.game.gui.notify("DRAW - ROLL AGAIN");
						} else{
							this.game.state = DiceGame.STATE_ROLL_ONE;
							this.game.gui.notify("New round - first roll");
						}
					}
				}).execute();
		}
	}

	public void executeReset(){
		// Reset the game
		this._reset();
	}


	public boolean checkWin(){
		if ((this.points.get(DiceGame.ID_PLAYER).intValue() >= this.winThreshold) || (this.points.get(DiceGame.ID_BOT).intValue() >= this.winThreshold)){
			if (this.points.get(DiceGame.ID_PLAYER).equals(this.points.get(DiceGame.ID_BOT))){
				// It's a draw
				this.state = DiceGame.STATE_ROLL_DRAW;
				JOptionPane.showMessageDialog(
					null,
					"It's a draw! Roll once.",
					"Draw!",
					JOptionPane.PLAIN_MESSAGE
				);
				return false;
			} else{
				// Someone won
				boolean playerWon = (this.points.get(DiceGame.ID_PLAYER).intValue() > this.points.get(DiceGame.ID_BOT).intValue())?(true):(false);
				JOptionPane.showMessageDialog(
					null,
					"Winner: " + ((playerWon)?("PLAYER"):("COMPUTER")),
					"Game has ended",
					(playerWon)?(JOptionPane.WARNING_MESSAGE):(JOptionPane.ERROR_MESSAGE)
				);
				if (playerWon){
					this._addWin();
				} else{
					this._addLoss();
				}
				this._reset();
				this.gui.notify("NEW GAME");
				return true;
			}
		}
		return false;
	}

	private int _getCurrentRollScore(int playerId){
		int ret = 0;
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			ret += this.getCurrentDices().get(playerId).get(i).getValue();
		}
		return ret;
	}

	public void displayUpdate(){
		// Display currently rolled dices
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			this.gui.updateDice(
				GraphicalInterface.ID_PLAYER,
				i,
				this.getCurrentDices().get(DiceGame.ID_PLAYER).get(i).getDieImage()
			);
			this.gui.updateDice(
				GraphicalInterface.ID_BOT,
				i,
				this.getCurrentDices().get(DiceGame.ID_BOT).get(i).getDieImage()
			);
		}
	}

	public void resetDisplay(boolean resetDiceDisplay){
		if (resetDiceDisplay){
			for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
				this.currentDices.get(DiceGame.ID_PLAYER).set(i, null);
				this.currentDices.get(DiceGame.ID_BOT).set(i, null);
			}
		}
		this.gui.resetDisplay(resetDiceDisplay);
	}

	private void _reset(){
		this.points.set(DiceGame.ID_PLAYER, 0);
		this.points.set(DiceGame.ID_BOT, 0);
		this.resetDisplay(true);
		this.state = DiceGame.STATE_ROLL_ONE;
		this.bot.reset();
		this.displayScore();
	}

	public void scoreCurrentRoll(int playerId){
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			this.points.set(playerId, this.points.get(playerId) + this.currentDices.get(playerId).get(i).getValue());
		}
	}

	public void displayPoints(){
		this.gui.setPoints(this.points.get(DiceGame.ID_BOT), this.points.get(DiceGame.ID_PLAYER));
	}

	public void displayInterimScore(){
		int interimPlayer = this.points.get(DiceGame.ID_PLAYER);
		int interimBot = this.points.get(DiceGame.ID_BOT);
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			interimPlayer += this.currentDices.get(DiceGame.ID_PLAYER).get(i).getValue();
			interimBot += this.currentDices.get(DiceGame.ID_BOT).get(i).getValue();

		}
		this.gui.setInterim(interimBot, interimPlayer);
	}

	public void checkboxHook(){
		// Disable the 'Throw' button if all checkboxes are checked (why rerollif we are
		// keeping all dices?) or if maximum allowed of rerolled dices are exceeded.
		int total = 0;
		ArrayList<Boolean> selected = this.gui.getSelected();
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			if (selected.get(i)){ total++; }
		}
		if (total >= Config.GAME_TOTAL_DICES){
			this.gui.enableThrow(false);
		} else{
			this.gui.enableThrow(((Config.GAME_TOTAL_DICES - total) <= Config.GAME_MAX_REROLLS)?(true):(false));
		}
	}


	private void displayScore(){
		ArrayList<Integer> score = this._getScore();
		this.gui.setScore(score.get(DiceGame.ID_PLAYER), score.get(DiceGame.ID_BOT));
	}

	private void _checkFile(){
		// Make sure the save file exists
		File f = new File(Config.PATH_SCORE);
		if (!f.exists()){
			try{
				PrintWriter w = new PrintWriter(Config.PATH_SCORE);
				w.println("0");
				w.println("0");
				w.close();
			} catch (FileNotFoundException e){
				System.out.println("FATAL: Could not open score file.");
				System.out.println("FATAL: Unrecoverable exception, exiting.");
				System.exit(1);
			}
		}
	}

	private ArrayList<Integer> _getScore(){
		// Get current wins and losses from the save file
		ArrayList<Integer> score = new ArrayList<Integer>();
		score.add(0); score.add(0);
		this._checkFile();
		try{
			Scanner fr = new Scanner(new File(Config.PATH_SCORE));
			for (int i = 0; i < 2; i++){
				score.set(i, fr.nextInt());
			}
			fr.close();
		} catch (FileNotFoundException e){
			System.out.println("FATAL: Could not open score file.");
			System.out.println("FATAL: Unrecoverable exception, exiting.");
			System.exit(1);
		}
		return score;
	}

	private void _addWin(){
		// Add a win in the save file
		ArrayList<Integer> score = this._getScore();
		try{
			PrintWriter w = new PrintWriter(Config.PATH_SCORE);
			w.println(score.get(0) + 1);
			w.println(score.get(1));
			w.close();
		} catch (FileNotFoundException e){
			System.out.println("FATAL: Could not open score file.");
			System.out.println("FATAL: Unrecoverable exception, exiting.");
			System.exit(1);
		}
	}

	private void _addLoss(){
		// Add a loss in the save file
		ArrayList<Integer> score = this._getScore();
		try{
			PrintWriter w = new PrintWriter(Config.PATH_SCORE);
			w.println(score.get(0));
			w.println(score.get(1) + 1);
			w.close();
		} catch (FileNotFoundException e){
			System.out.println("FATAL: Could not open score file.");
			System.out.println("FATAL: Unrecoverable exception, exiting.");
			System.exit(1);
		}
	}

	// In case any filtering / validation requirements arise later in the development
	public int getState(){ return this.state; }
	public void setState(int s){ this.state = s; }
	public int getWinThreshold(){ return this.winThreshold; }
	public ArrayList<Integer> getPoints(){ return this.points; }
	public ArrayList<ArrayList<Die>> getCurrentDices(){ return this.currentDices; }

	/* === INIT FUNCTIONS === */
	private void _initHooks(){
		// Hook the correct action listeners
		this.gui.hookThrow(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.game.executeThrow();
			}
		});

		this.gui.hookScore(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.game.executeScore();
			}
		});

		this.gui.hookReset(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.game.executeReset();
			}
		});

		this.gui.hookCheckboxes(new DiceActionListener(this){
			@Override
			public void actionPerformed(ActionEvent e){
				this.game.gui.checkboxHook();
			}
		});
	}
}
