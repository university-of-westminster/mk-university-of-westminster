package uow.game.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import uow.game.util.Config;
import uow.game.util.Logger;

import uow.game.DiceGame;

/*
 * This file contains the AI of the bot player
 *
 * The bot uses very simple decision flow for it's decisions. First it passes the dices through a number
 * of filters that decide whether a dice should be rerolled or not. Afterwards, if there are any untouched
 * dices left by the filters, it applies a probability of a reroll for each of them based on their value.
 * This flow can be illustrated as follows:
 *   All Dices -> filter(1) -> filter(2) -> ... -> filter(n) -> probabilityCalc() -> reroll()
 *
 * The filtering system is fully modular and allows arbitrary sequences. You can implement you own filter
 * in the _filter() function by creating a new switch case, assigning your algorithm an ID and finally
 * queuing the algorithm for execution at the end of the constructor with this.filters.add(ID).
 *
 * There are also multiple probability calculations available, you can select any one of them by changing
 * the ALGO_PROBABILITY constant to the specified ID
 *
 * A little more description for each filtering and probability algorithms can be found in various places
 * lower down in this file.
 *
 * CURRENT STRATEGY:
 *   The currently running strategy of the bot is following:
 *     1: Remove any dices that could bring you below winning point if rerolled (if we are above already)
 *     2: If we have a really good roll already (many 6's or 5's), reroll even low-probability high
 *        value dices we have (eg. 4's or 5's).
 *     3: Apply a probability calculation formula to any leftover dices. The currently selected formula
 *        closely follows my own decisions when rerolling (eg. high probability of rerolling a 2 or 3, but
 *        low probability to reroll a 4).
 *
 *     I believe the chosen strategy is very solid considering the overall difficulty of this coursework and
 *     my quite sub-par mathematical skills. It generally follows a pretty safe reroll path, while also
 *     being able to take risks once in a while.
 *
 *
 * Note: The bot continues to use his remaining rerolls if the player decides to end the turn early (by
 * scoring their current roll). It also does not continue to play in the current round if it already decided
 * it's roll is good enough.
 */

public class DiceBot{
	private DiceGame game;
	private int roll;
	private int state;
	private int filterState;
	private ArrayList<Integer> filters;
	private Random rng;

	/*
	 * REROLL ALGORITHM SELECTION
	 *
	 * PROBABILITY:
	 *   Available algorithms:
	 *     -1: Reroll everything below 4, keep otherwise
	 *      1: Reroll anything below 4, keep 6. Reroll 4 and 5 with okay-ish probability
	 *      2: Reroll 1, 2, 3 and 4 with very optimal probability, keep 5 and 6
	 *
	 *   WARNING: Read the required constraints of each algorithm in the comment above _probabilityCalc()
	 *            [constraints: ALGO_LOW_CUTOFF and ALGO_HIGH_CUTOFF constants]
	 *            Failure to meet specified algorithm constraints results in undefined behaviour.
	 *
	 * FILTERING:
	 *   Available algorithms:
	 *     -1: No filtering
	 *      1: Ensure a reroll doesn't bring you below win point (if already above)
	 *      2: Always reroll high rolls (4,5) in case of a very good current round score
	 */

	public static final int ALGO_PROBABILITY =  2;
	public static final int ALGO_PROBABILITY_LOW_CUTOFF  = 1;
	public static final int ALGO_PROBABILITY_HIGH_CUTOFF = 5;
	public static final double ALGO_PROBABILITY_ALWAYS   =  2.0;
	public static final double ALGO_PROBABILITY_NEVER    = -1.0;
	public static final double ALGO_PROBABILITY_UNKNOWN  = 999.0;
	public static final double ALGO_PROBABILITY_DEFAULT  = DiceBot.ALGO_PROBABILITY_ALWAYS;
	public static final double ALGO_FILTER_CHOICE_REMOVE = 100.0;
	public static final double ALGO_FILTER_CHOICE_KEEP   = DiceBot.ALGO_PROBABILITY_NEVER;
	public static final double ALGO_FILTER_CHOICE_REROLL = DiceBot.ALGO_PROBABILITY_ALWAYS;
	public static final double ALGO_FILTER_CHOICE_SKIP   = DiceBot.ALGO_PROBABILITY_UNKNOWN;
	public static final int ALGO_FILTER_STATE_RUNNING    = 1;
	public static final int ALGO_FILTER_STATE_FINISHED   = 2;

	public DiceBot(DiceGame g){
		this.game = g;
		this.roll = 1;
		this.state = DiceGame.STATE_ROLL_ONE;
		this.rng = new Random();

		/*
		 * FILTER QUEUE
		 *
		 * Filters added below are ran sequentially in the specified order
		 */
		this.filters = new ArrayList<Integer>();
		this.filters.add(1);
		this.filters.add(2);
	}

	public void play(){
		switch(this.state){
			case DiceGame.STATE_ROLL_ONE:
				this.game.reroll(DiceGame.ID_BOT);
				this.state = DiceGame.STATE_ROLL_TWO;
				break;
			case DiceGame.STATE_ROLL_TWO:
			case DiceGame.STATE_ROLL_THREE:
				this._play();
		}
	}

	public void finish(){
		while ((this.state != DiceGame.STATE_ROLL_FINISHED) && (this.state != DiceGame.STATE_ROLL_ONE)){
			this._play();
		}
		this.state = DiceGame.STATE_ROLL_ONE;
	}

	public void reset(){
		this.state = DiceGame.STATE_ROLL_ONE;
	}


	private void _play(){
		//float modifier = (this.state == DiceGame.STATE_ROLL_TWO)?(1.0):(0.5);

		// Get list of dices we might want to reroll
		ArrayList<RerollProbabilityPair> probabilities = this._constructProbabilityList();

		ArrayList<Boolean> kept = new ArrayList<Boolean>();
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){ kept.add(true); }

		int totalRerolled = 0;
		for (int i = 0; i < probabilities.size(); i++){
			// Set the specified dice to be rerolled in case probability check passes
			if (rng.nextDouble() <= probabilities.get(i).probability){
				kept.set(probabilities.get(i).index, false);
				totalRerolled++;
				Logger.L("Rerolling index [" + probabilities.get(i).index + "] - probability [" + probabilities.get(i).probability +"]");
			}
		}
		if (totalRerolled > 0){
			// Actually reroll the dices set to be rerolled
			this.game.reroll(DiceGame.ID_BOT, kept);
			this.state = ((this.state == DiceGame.STATE_ROLL_TWO)?(DiceGame.STATE_ROLL_THREE):(DiceGame.STATE_ROLL_FINISHED));
			Logger.L("Bot rerolled " + totalRerolled + " dices");
		}
		else{
			this.state = DiceGame.STATE_ROLL_FINISHED;
			Logger.L("Bot finished this turn and will not reroll again this round.");
		}
	}


	private ArrayList<RerollProbabilityPair> _constructProbabilityList(){
		ArrayList<RerollProbabilityPair> ret = new ArrayList<RerollProbabilityPair>();
		for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
			ret.add(new RerollProbabilityPair(
				i,
				DiceBot.ALGO_PROBABILITY_UNKNOWN
			));
		}

		// Filter queue
		for (int i =0; i < this.filters.size(); i++){
			Logger.L("Running filter [" + i + "]");
			this.filterState = DiceBot.ALGO_FILTER_STATE_RUNNING;
			int index = 0;
			while (index < ret.size()){
				if (this.filterState == DiceBot.ALGO_FILTER_STATE_RUNNING){
					double result = this._filter(this.filters.get(i).intValue(), ret.get(index).index);
					Logger.L("Filter result for index [" + ret.get(index).index + "] = " + result);
					if (result == DiceBot.ALGO_FILTER_CHOICE_REMOVE){
						ret.remove(index);
						index = -1;
					} else if (result == DiceBot.ALGO_FILTER_CHOICE_SKIP){
						// Do nothing
					}else{
						ret.get(i).probability = result;
					}
				} else{ Logger.L("Current filter indicated it has finished, skipping cycle"); }
				index++;
			}
		}

		// Set untouched probabilities 
		this._setDiceGameProbabilities(ret);

		Collections.sort(ret);
		this._limitReroll(ret);
		
		return ret;
	}

	private void _setDiceGameProbabilities(ArrayList<RerollProbabilityPair> p){
		for (int i = 0; i < p.size(); i++){
			if (p.get(i).probability == DiceBot.ALGO_PROBABILITY_UNKNOWN){
				p.get(i).probability = this._probabilityCalc(
					this.game.getCurrentDices().get(DiceGame.ID_BOT).get(p.get(i).index).getValue()
				);
			}
		}
	}

	private void _limitReroll(ArrayList<RerollProbabilityPair> p){
		// Make sure not to cheat
		while (p.size() >Config.GAME_MAX_REROLLS){ p.remove(p.size()-1); }
	}



	/*
	 * Filtering algorithms
	 */
	private double _filter(int algo, int index){
		int currentRoll = 0;
		int totalCurrent = 0;
		switch(algo){
			case -1:
				return DiceBot.ALGO_FILTER_CHOICE_SKIP;
			case  1:
				totalCurrent = this.game.getPoints().get(DiceGame.ID_BOT).intValue();
				for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
					currentRoll += this.game.getCurrentDices().get(DiceGame.ID_BOT).get(index).getValue();
				}
				totalCurrent += currentRoll;

				if (totalCurrent < this.game.getWinThreshold()){
					return DiceBot.ALGO_FILTER_CHOICE_SKIP;
				}

				if (
					(totalCurrent - this.game.getCurrentDices().get(DiceGame.ID_BOT).get(index).getValue())
					<
					this.game.getWinThreshold()
				){
					return DiceBot.ALGO_FILTER_CHOICE_REMOVE;
				}
				return DiceBot.ALGO_FILTER_CHOICE_SKIP;
			case  2:
				for (int i = 0; i < Config.GAME_TOTAL_DICES; i++){
					currentRoll += this.game.getCurrentDices().get(DiceGame.ID_BOT).get(index).getValue();
				}
				
				if (currentRoll >= 24){
					if (this.game.getCurrentDices().get(DiceGame.ID_BOT).get(index).getValue() < 6){
						this.filterState = DiceBot.ALGO_FILTER_STATE_FINISHED;
						return DiceBot.ALGO_FILTER_CHOICE_REROLL;
					}
				} else if (currentRoll >= 20){
					if (this.game.getCurrentDices().get(DiceGame.ID_BOT).get(index).getValue() < 5){
						this.filterState = DiceBot.ALGO_FILTER_STATE_FINISHED;
						return DiceBot.ALGO_FILTER_CHOICE_REROLL;
					}
				}
				return DiceBot.ALGO_FILTER_CHOICE_SKIP;
		}
		return DiceBot.ALGO_FILTER_CHOICE_SKIP;
	}

	/*
	 * Reroll probability algorithms
	 *
	 * Probability algorithm constraints (inclusive - cutoff constants are exclusive so add or 
	 * substract 1 from these values):
	 *  -1:
	 *      MIN: 1
	 *      MAX: 6
	 *   1:
	 *      MIN: 1
	 *      MAX: 6
	 *   2:
	 *      MIN: 2
	 *      MAX: 4
	 *
	 *   When using these algorithms _DO NOT_ exceed the given constraints in ALGO_{HIGH,LOW}_CUTOFF!
	 */
	private double _probabilityCalc(int x){
		if (x >= DiceBot.ALGO_PROBABILITY_HIGH_CUTOFF){ return DiceBot.ALGO_PROBABILITY_NEVER; }
		if (x <= DiceBot.ALGO_PROBABILITY_LOW_CUTOFF){ return DiceBot.ALGO_PROBABILITY_ALWAYS; }

		switch (DiceBot.ALGO_PROBABILITY){
			case -1:
				if (x > 3){ return DiceBot.ALGO_PROBABILITY_NEVER;}
				else{ return DiceBot.ALGO_PROBABILITY_ALWAYS; }
			case  1:
				return (
					  (-0.00729167 * Math.pow(x, 4))
					+ (0.127546 * Math.pow(x, 3))
					- (0.735069 * Math.pow(x, 2))
					+ (1.35152 * x)
					+ (0.258333)
				);
			case  2:
				return (
					  (0.584434 * Math.sin(x))
					- (0.520957 * Math.cos(x))
					+ (0.201781)
				);
		}
		return DiceBot.ALGO_PROBABILITY_DEFAULT;
	}
}


class RerollProbabilityPair implements Comparable<RerollProbabilityPair>{
	public int index;
	public double probability;
	public RerollProbabilityPair(int i, double p){
		this.index = i;
		this.probability = p;
	}

	public int compareTo(RerollProbabilityPair other){
		return Double.compare(other.probability, this.probability);
	}
}
