package uow.game;

import java.util.ArrayList;
import java.util.Random;

import uow.game.util.Config;

import uow.game.Die;

public class Dice{
	private ArrayList<Die> dies;
	private Random rng;

	public Dice(){
		this.dies = new ArrayList<Die>();

		// Add dies, one 'unknown' die in case we need it later
		for (int i = 0; i < 7; i++){
			this.dies.add(new Die(i));
		}

		this.rng = new Random();
	}

	public ArrayList<Die> roll(){
		return this.roll(Config.GAME_TOTAL_DICES);
	}

	public ArrayList<Die> roll(int num){
		ArrayList<Die> ret = new ArrayList<Die>();
		for (int i = 0; i < num; i++){
				if (Config.DEBUG_RIGGED){ ret.add(this.dies.get(6)); }
				else { ret.add(this.dies.get(this.rng.nextInt(6) + 1)); }
		}
		return ret;
	}
}
