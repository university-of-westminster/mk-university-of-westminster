use strict;
use warnings;

my $total = 0;

while (<>){
	if ($_ =~ m~(private|public|protected) [a-zA-Z<>]* [a-zA-Z_]*\([a-zA-Z<>, ]*\)~){
		$total++;
		#print $_;
	}
}

print "Total function in the file: $total\n";
