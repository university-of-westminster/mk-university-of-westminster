#!/usr/bin/env python
import sys
import copy
import random

if (len(sys.argv) != 2):
	print "USAGE:", sys.argv[0], "<iterations>"
	exit()

try:
	int(sys.argv[1])
except ValueError:
	print "ERROR: Iteration value has to be an integer value"
	exit()

ITER  = int(sys.argv[1])
locations = ['Earth', 'Moon', 'Saturn', 'Neptune', 'Sun']
clubs = [
		'Liverpool',
		'Chelsea',
		'Barcelona',
		'Arsenal',
		'Manchester United',
		'Real Madrid'
	]
dates = [
		{
			'year'  : 2000,
			'month' : 1,
			'day'   : 1,
			'future': False
		},{
			'year'  : 2000,
			'month' : 'February',
			'day'   : 28,
			'future': False
		},{
			'year'  : 2000,
			'month' : 'December',
			'day'   : 31,
			'future': False
		},{
			'year'  : 2015,
			'month' : 1,
			'day'   : 1,
			'future': False
		},{
			'year'  : 2015,
			'month' : 'June',
			'day'   : 1,
			'future': False
		},{
			'year'  : 2015,
			'month' : 12,
			'day'   : 31,
			'future': True
		}
	]

def makeTeams():
	for team in clubs:
		print "C"
		print team
		print random.choice(locations)
		print ""

def randomDate():
	tmp = copy.deepcopy(dates)
	random.shuffle(tmp)
	x = tmp.pop()
	print x['year']
	print x['month']
	print x['day']
	return x['future']

def finalize():
	print "P"
	print ""
	print "sj"
	print ""
	print "X"

def main():
	makeTeams()
	for i in range(0, ITER):
		tmp = copy.deepcopy(clubs);
		random.shuffle(tmp)
		print "A"
		if (randomDate()):
			for j in range(0, 2):
				print tmp.pop()
		else:
			for j in range(0, 2):
				print tmp.pop()
				print int(random.uniform(0, 7))
		print ""
	finalize()

main()
