package uow.club;

import uow.club.SportsClub;

public class FootballClub extends SportsClub{
	public static enum Result{WIN, DRAW, DEFEAT}

	private int goalsScored		= 0;
	private int goalsReceived	= 0;
	private int points		= 0;
	private int matchesPlayed	= 0;


	public FootballClub(String name){
		this.name = name;
	}

	public FootballClub(String name, String location){
		this.name = name;
		this.location = location;
	}


	public void addMatch(Result result, int gScr, int gRcv){
		boolean cont = true;
		switch (result){
			case WIN:
				this._win();
				break;
			case DRAW:
				this._draw();
				break;
			case DEFEAT:
				this._defeat();
				break;
			default:
				System.out.println("ERROR: Invalid result");
				cont = false;
				break;
		}
		if (cont){
			this._scoreGoals(gScr);
			this._receiveGoals(gRcv);
			this.matchesPlayed++;
		}
	}


	private void _scoreGoals(int goals){ this.goalsScored += goals; }
	private void _receiveGoals(int goals){ this.goalsReceived += goals; }

	private void _win(){
		this.wins++;
		this.points += 3;
	}
	private void _draw(){
		this.draws++;
		this.points += 1;
	}
	private void _defeat(){
		this.defeats++;
	}

	@Override
	public void printStats(){
		System.out.println(".=== STATISTICS FOR TEAM " + this.name + " ===");
		System.out.println("| Location: " + this.location);
		System.out.println("|--------------------");
		System.out.println("| Matches played: " + this.matchesPlayed);
		System.out.println("|--------------------");
		System.out.println("| Points: " + this.points);
		System.out.println("|--------------------");
		System.out.println("| Wins    : " + this.wins);
		System.out.println("| Draws   : " + this.draws);
		System.out.println("| Defeats : " + this.defeats);
		System.out.println("|--------------------");
		System.out.println("| Goals scored  : " + this.goalsScored);
		System.out.println("| Goals received: " + this.goalsReceived);
		System.out.println("'--------------------");
	}


	public int getGoalsScored(){ return this.goalsScored; }
	public int getGoalsReceived(){ return this.goalsReceived; }
	public int getPoints(){ return this.points; }
	public int getMatchesPlayed(){ return this.matchesPlayed; }
}
