package uow.club;

public abstract class SportsClub{
	protected String name;
	protected String location;

	protected int wins;
	protected int draws;
	protected int defeats;

	public abstract void printStats();

	public String getName(){ return this.name; }
	public String getLocation(){ return this.location; }
	public int getWins(){ return this.wins; }
	public int getDraws(){ return this.draws; }
	public int getDefeats(){ return this.defeats; }
}
