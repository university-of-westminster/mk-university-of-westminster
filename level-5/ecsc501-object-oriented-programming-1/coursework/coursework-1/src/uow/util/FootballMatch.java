package uow.util;

import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

public class FootballMatch{
	public FootballTeamMatch one;
	public FootballTeamMatch two;

	private GregorianCalendar date;

	public FootballMatch(GregorianCalendar g){ this.date = g; }

	public void setOne(String name){
		this.one = new FootballTeamMatch(name, 0, 0);
	}

	public void setOne(String name, int received, int scored){
		this.one = new FootballTeamMatch(name, received, scored);
	}

	public void setTwo(String name){
		this.two = new FootballTeamMatch(name, 0, 0);
	}

	public void setTwo(String name, int received, int scored){
		this.two = new FootballTeamMatch(name, received, scored);
	}

	public String getDate(){
		return new SimpleDateFormat("d. MMMM yyyy").format(this.date.getTime());
	}

	public GregorianCalendar getDateRaw(){
		return this.date;
	}

	public Hashtable<String, Integer> getDateHash(){
		Hashtable<String, Integer> tmp = new Hashtable<String, Integer>();
		tmp.put("year", Integer.parseInt(new SimpleDateFormat("yyyy").format(this.date.getTime())));
		tmp.put("month", Integer.parseInt(new SimpleDateFormat("M").format(this.date.getTime())));
		tmp.put("day", Integer.parseInt(new SimpleDateFormat("d").format(this.date.getTime())));
		return tmp;
	}
}
