package uow.util;

import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

public class FootballTeamMatch{
	private String name;
	private int received;
	private int scored;

	public FootballTeamMatch(String n, int r, int s){
		this.name = n;
		this.received = r;
		this.scored = s;
	}

	public String getName(){ return this.name; }
	public int getScored(){ return this.scored; }
	public int getReceived(){ return this.received; }
}
