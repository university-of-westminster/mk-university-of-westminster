package uow.util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.DateFormatSymbols;

public class CalendarPrinter{
	private GregorianCalendar calendar;

	public CalendarPrinter(){
		this.calendar = new GregorianCalendar();
		this.calendar.set(Calendar.DATE, 1);
		this.calendar.set(Calendar.HOUR_OF_DAY, 0);
		this.calendar.set(Calendar.MINUTE, 0);
		this.calendar.set(Calendar.SECOND, 0);
		this.calendar.set(Calendar.MILLISECOND, 0);
	}

	public CalendarPrinter(int year, int month){
		this.calendar = new GregorianCalendar(year, month - 1, 1);
		//this.calendar.complete();
		//this.calendar.clear(Calendar.Year); this.calendar.clear(Calendar.MONTH); this.calendar.clear(Calendar.DATE);
		//this.calendar.set(Calendar.YEAR, year);
		//this.calendar.set(Calendar.MONTH, month);
		//this.calendar.set(Calendar.DATE, 1);
	}


	public void print(){
		int lastDay  = this.dayMax();	
		int startDay = this.calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (startDay < 1){ startDay = 7; }
		int day = startDay;
		int x = 0;
		StringBuilder title;
		StringBuilder line = new StringBuilder();

		title = new StringBuilder(new DateFormatSymbols().getMonths()[this.calendar.get(Calendar.MONTH)]);
		title.append(" "); title.append(this.calendar.get(Calendar.YEAR));
		x = title.length();
		for (int i = 0; i < ((20 - x) / 2); i++){ title.insert(0, " "); }
		// while (title.length() <= 20){ title.append(" "); // right side centering not required if line is empty afterward
		System.out.println(title);
		System.out.println("Mo Tu We Th Fr Sa Su");
		for (int i = 0; i < (startDay - 1); i++){ line.append("   "); }
		while (day < (lastDay + startDay)){
			line.append(String.format("%2s", day - (startDay - 1)));
			if ((day % 7) == 0){
				System.out.println(line);
				line.delete(0, line.length());
			} else{
				line.append(" ");
			}
			day++;
		}
		if (line.length() > 0){ System.out.println(line); }
	}

	public int dayMax(){ return this.calendar.getActualMaximum(Calendar.DAY_OF_MONTH); }
	public int dayMin(){ return this.calendar.getActualMinimum(Calendar.DAY_OF_MONTH); }

	public boolean isFuture(){
		GregorianCalendar today = new GregorianCalendar();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		if (this.calendar.after(today)){ return true; }
		return false;
	}
}
