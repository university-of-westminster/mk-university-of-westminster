package uow.league;

import java.util.ArrayList;
import uow.exceptions.InvalidClubPropertyException;

public interface LeagueManager{
	public void displayMenu();
	public void printStats(String name);
}
