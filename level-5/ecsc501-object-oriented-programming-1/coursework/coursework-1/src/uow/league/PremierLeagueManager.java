package uow.league;

import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Comparator;
import java.util.Scanner;

import java.io.FileWriter;

import uow.league.LeagueManager;
import uow.club.FootballClub;

import uow.util.FootballMatch;
import uow.util.CalendarPrinter;

import uow.exceptions.*;

public class PremierLeagueManager implements LeagueManager{
	private ArrayList<FootballClub> clubs;
	private ArrayList<FootballMatch> matches;
	private Scanner in;
	private boolean running;

	public PremierLeagueManager(){
		this.clubs = new ArrayList<FootballClub>();
		this.matches = new ArrayList<FootballMatch>();
		this.in = new Scanner(System.in);
		this.running = true;
		//this.in.useDelimiter(System.getProperty("line.separator"));
	}

	public boolean isRunning(){ return this.running; }


	@Override
	public void displayMenu(){
		String selection = "";
		System.out.println(".=== PREMIER LEAGUE MANAGER ===.");
		System.out.println("| Please select an option      |");
		System.out.println("|------------------------------|");
		System.out.println("|  [C]reate a club             |");
		System.out.println("|  [R]emove a club             |");
		System.out.println("|  [A]dd a match               |");
		System.out.println("|  [P]remier table             |");
		System.out.println("|  [L]ist all clubs            |");
		System.out.println("|  [F]ind matches              |");
		System.out.println("|  [T]eam statistics           |");
		System.out.println("|------------------------------|");
		System.out.println("| JSON Persistence:            |");
		System.out.println("|  [SJ] Save the current state |");
		System.out.println("|  [LJ] Load state from file   |");
		System.out.println("|------------------------------|");
		System.out.println("|  E[x]it                      |");
		System.out.println("'------------------------------'");
		System.out.println();
		System.out.print("Selection: ");

		selection = this.in.nextLine().toLowerCase().trim();
		if (selection.equals("x")){
			System.out.println("Exiting, bye!");
			this.running = false;
			return;
		} else if (selection.equals("c")){
			this.promptCreateClub();
		} else if (selection.equals("r")){
			this.promptRemoveClub();
		} else if (selection.equals("a")){
			this.promptAddMatch();
		} else if (selection.equals("p")){
			this.printPremierTable();
		} else if (selection.equals("l")){
			this.printClubList();
		} else if (selection.equals("f")){
			this.promptMatchSearch();
		} else if (selection.equals("t")){
			this.promptPrintStats();
		} else if (selection.equals("sj")){
			this._saveJSON();
		} else if (selection.equals("lj")){
			this._loadJSON();
		} else{
			System.out.println("ERROR: Invalid option, try again");
			return;
		}
		

		this._block();
	}

	public void promptCreateClub(){
		String name;
		String location;
		System.out.println("*** CREATING CLUB ***");
		System.out.print("Name    : ");
		name = this.in.nextLine();

		try{
			this._findClub(name);
			System.out.println("ERROR: This club already exists");
			return;
		} catch (ClubNotFoundException e){}

		System.out.print("Location: ");
		location = this.in.nextLine();
		try{
			this.createClub(name, location);
			System.out.println("Club " + this.clubs.get(this.clubs.size()-1).getName() + " created");
		} catch (InvalidClubPropertyException e){
			System.out.println("ERROR: Location or name is invalid");
		}
	}

	public void promptRemoveClub(){
		System.out.println("*** REMOVING CLUB ***");
		System.out.print("Name: ");
		this.removeClub(this.in.nextLine());
	}

	public void promptPrintStats(){
		System.out.print("Team name: ");
		this.printStats(this.in.nextLine());
	}

	public void promptAddMatch(){
		String club1;
		String club2;
		int scored1;
		int scored2;
		Hashtable<String, Integer> date;
		System.out.println("*** ADDING A MATCH ***");
		System.out.println("Date of the match:");
		try{
			date = this._getDate();
		} catch(InvalidDateException e){
			System.out.println("ERROR: The entered date is not valid");
			return;
		}
		try{
			if (this._isFuture(date.get("year"), date.get("month"), date.get("day"))){
				System.out.println("This match is in the future, you cannot specify the result");
				System.out.print("Name [1]: ");
				club1 = this.in.nextLine();
				club1 = this.clubs.get(this._findClub(club1)).getName();
				System.out.print("Name [2]: ");
				club2 = this.in.nextLine();
				club2 = this.clubs.get(this._findClub(club2)).getName();
				this.createMatch(new GregorianCalendar(date.get("year"), date.get("month"), date.get("day")), club1, club2);
			} else{
				try{
					System.out.println("This match is in the past, you must enter the result");
					System.out.print("Name [1]: ");
					club1 = this.in.nextLine();
					club1 = this.clubs.get(this._findClub(club1)).getName();
					System.out.print("  Goals scored [1]: ");
					scored1 = Integer.parseInt(this.in.nextLine());

					System.out.print("Name [2]: ");
					club2 = this.in.nextLine();
					club2 = this.clubs.get(this._findClub(club2)).getName();
					System.out.print("  Goals scored [2]: ");
					scored2 = Integer.parseInt(this.in.nextLine());

					this.createMatch(new GregorianCalendar(date.get("year"), date.get("month"), date.get("day")), club1, scored1, club2, scored2);
				} catch (NumberFormatException e){
					System.out.println("Invalid number");
					return;
				}
			}
		} catch (ClubNotFoundException e){
			System.out.println("ERROR: The specified club wasn't found");
			return;
		}
		System.out.println("Match created");
	}

	public void promptMatchSearch(){
		Hashtable<String, Integer> date;
		String day;
		try{
			date = this._getDate();
		} catch (InvalidDateException e){
			System.out.println("ERROR: The entered date is not valid");
			return;
		}

		ArrayList<Integer> matchIndexes = this._findMatches(date.get("year"), date.get("month"), date.get("day"));
		if (matchIndexes.size() < 1){
			System.out.println("There are no matches on this day");
		} else{
			for (int i = 0; i < matchIndexes.size(); i++){
				this.printMatch(matchIndexes.get(i));
			}
		}
	}

	public void printClubList(){
		System.out.println("The premier league currently consists of the following clubs:");
		if (this.clubs.size() < 1){ System.out.println("   There are currently no clubs in the premier league"); }
		else{
			for (int i = 0; i < this.clubs.size(); i++){
				System.out.println("  - " + this.clubs.get(i).getName());
			}
		}
	}

	public void createClub(String name, String location) throws InvalidClubPropertyException{
		if (name.trim().equals("") || location.trim().equals("")){
			throw new InvalidClubPropertyException();
		}
		this.clubs.add(new FootballClub(name.trim(), location.trim()));
	}

	public void removeClub(String name){
		try{
			if (this.clubs.get(this._findClub(name)).getMatchesPlayed() > 0){
				System.out.println("WARNING: This club has match entries, are you sure? (match entries will stay)");
				System.out.print("Continue [y/n]: ");
				if (!this.in.nextLine().trim().toLowerCase().equals("y")){
					System.out.println("Aborting...");
					return;
				}
			}
			this.clubs.remove(this._findClub(name));
			System.out.println("Club '" + name + "' removed");
		} catch (ClubNotFoundException e){
			System.out.println("ERROR: Club with the specified name wasn't found");
		}
	}

	public void printMatch(int index){
		try{
			FootballMatch m = this.matches.get(index);

			System.out.println("MATCH STATISTICS [" + m.getDate() + "]");
			System.out.println("Clubs : " + m.one.getName() + " VS " + m.two.getName());

			if (this._isFuture(m.getDateRaw())){
				System.out.println("This match has not been played yet, no results available");
			} else{
				int won;
				if (m.one.getScored() > m.two.getScored()){ won = 1; }
				else if (m.two.getScored() > m.one.getScored()){ won = 2; }
				else{ won = 0; }

				System.out.print("Winner: ");
				if (won == 1){ System.out.println(m.one.getName()); }
				else if (won == 2){ System.out.println(m.two.getName()); }
				else{ System.out.println("DRAW"); }
	
				System.out.println("Statistics for team '" + m.one.getName() + "':");
				System.out.println("  - Goals scored   : " + m.one.getScored());
				System.out.println("  - Goals received : " + m.one.getReceived());

				System.out.println("Statistics for team '" + m.two.getName() + "':");
				System.out.println("  - Goals scored   : " + m.two.getScored());
				System.out.println("  - Goals received : " + m.two.getReceived());
			}
			System.out.println("------------------");
		} catch (Exception e){
			System.out.println("ERROR: Match with the specified ID does not exist");
		}
	}

	public void createMatch(GregorianCalendar date, String club1, String club2){
		FootballMatch tmp = new FootballMatch(date);
		tmp.setOne(club1);
		tmp.setTwo(club2);
		this.matches.add(tmp);
	}

	public void createMatch(GregorianCalendar date, String team1, int scored1, String team2, int scored2){
		int team1Id = -1;
		int team2Id = -1;
		FootballMatch tmp = new FootballMatch(date);

		try{
			team1Id = this._findClub(team1);
			team2Id = this._findClub(team2);
		} catch (ClubNotFoundException e){
			System.out.println("ERROR: Club with this name does not exist");
		}

		tmp.setOne(team1, scored2, scored1);
		tmp.setTwo(team2, scored1, scored2);
		if (scored1 > scored2){
			this.clubs.get(team1Id).addMatch(FootballClub.Result.WIN, scored1, scored2);
			this.clubs.get(team2Id).addMatch(FootballClub.Result.DEFEAT, scored2, scored1);
		} else if (scored1 == scored2){
			this.clubs.get(team1Id).addMatch(FootballClub.Result.DRAW, scored1, scored2);
			this.clubs.get(team2Id).addMatch(FootballClub.Result.DRAW, scored2, scored1);
		} else{
			this.clubs.get(team1Id).addMatch(FootballClub.Result.DEFEAT, scored1, scored2);
			this.clubs.get(team2Id).addMatch(FootballClub.Result.WIN, scored2, scored1);
		}
		this.matches.add(tmp);
		this._sort();
	}

	@Override
	public void printStats(String name){
		try{
			this.clubs.get(this._findClub(name)).printStats();
		} catch (ClubNotFoundException e){
			System.out.println("ERROR: Team with name '" + name + "' does not exist");
		}
	}

	public void printPremierTable(){
		String format = "| %-20s | %7s | %7s | %7s | %7s | %7s | %8s |";
		System.out.println("Team sorted by performance:");
		//System.out.println("      Team         Points  Scored  Received");
		System.out.println(".-----------------------------------------------------------------------------------.");
		System.out.println(String.format(format, "Team", "Wins", "Draws", "Defeats", "Points", "Scored", "Received"));
		System.out.println("|-----------------------------------------------------------------------------------|");
		for (int i = 0; i < this.clubs.size(); i++){
			FootballClub c = this.clubs.get(i);
			System.out.println(String.format(format, c.getName(), c.getWins(), c.getDraws(), c.getDefeats(), c.getPoints(), c.getGoalsScored(), c.getGoalsReceived()));
		}
		if (this.clubs.size() < 1){ System.out.println(String.format("| %-81s |", "There are currently no clubs in the premier league")); }
		System.out.println("'-----------------------------------------------------------------------------------'");
	}


	private Hashtable<String, Integer> _getDate() throws InvalidDateException{
		String year;
		String month;
		Hashtable<String, Integer> date = new Hashtable<String, Integer>();

		System.out.print("Year [1995-2035]: ");
		year = this.in.nextLine().trim();
		try{ date.put("year", Integer.parseInt(year)); } catch (NumberFormatException e){ throw new InvalidDateException(); }
		if ((date.get("year") < 1995) || (date.get("year") > 2035)){
			throw new InvalidDateException();
		}

		System.out.print("Month [January-December | 1-12]: ");
		month = this.in.nextLine().trim();
		try{
			int tmp = Integer.parseInt(month);
			date.put("month", tmp);
		} catch (NumberFormatException e){
			date.put("month", this._mapMonth(month));
		}
		if ((date.get("month") < 1) || (date.get("month") > 12)){
			throw new InvalidDateException();
		}
		date.put("month", date.get("month")-1);

		CalendarPrinter c = new CalendarPrinter(date.get("year"), date.get("month")+1);
		c.print();

		System.out.print("Select day: ");
		String day = this.in.nextLine().trim();
		try{
			date.put("day", Integer.parseInt(day));
			if ((date.get("day") < c.dayMin()) || date.get("day") > c.dayMax()){
				throw new InvalidDateException();
			}
		} catch (NumberFormatException e){
			throw new InvalidDateException();
		}

		return date;
	}

	private int _findClub(String name) throws ClubNotFoundException{
		for (int i = 0; i < this.clubs.size(); i++){
			if (this.clubs.get(i).getName().toLowerCase().equals(name.toLowerCase().trim())){
				return i;
			}
		}
		throw new ClubNotFoundException();
	}

	private ArrayList<Integer> _findMatches(int year, int month, int day){
		ArrayList<Integer> result = new ArrayList<Integer>();
		GregorianCalendar c = new GregorianCalendar(year, month, day);
		for (int i = 0; i < this.matches.size(); i++){
			if (c.equals(this.matches.get(i).getDateRaw())){
				result.add(i);
			}
		}
		return result;
	}

	private int _mapMonth(String month){
		month = month.toLowerCase().trim();
		if (month.equals("january")){ return 1; }
		else if (month.equals("february")){ return 2; }
		else if (month.equals("march")){ return 3; }
		else if (month.equals("april")){ return 4; }
		else if (month.equals("may")){ return 5; }
		else if (month.equals("june")){ return 6; }
		else if (month.equals("july")){ return 7; }
		else if (month.equals("august")){ return 8; }
		else if (month.equals("september")){ return 9; }
		else if (month.equals("october")){ return 10; }
		else if (month.equals("november")){ return 11; }
		else if (month.equals("december")){ return 12; }

		return -1;
	}

	private boolean _isFuture(GregorianCalendar date){
		GregorianCalendar today = new GregorianCalendar();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);

		if (date.after(today)){ return true; }
		return false;
	}

	private boolean _isFuture(int year, int month, int day){
		GregorianCalendar today = new GregorianCalendar();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);

		GregorianCalendar date = new GregorianCalendar(year, month, day);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);

		if (date.after(today)){ return true; }
		return false;
	}

	private void _saveJSON(){ // Tutor requested JSON persistence
		org.json.simple.JSONObject json = new org.json.simple.JSONObject(); // root object

		org.json.simple.JSONArray c = new org.json.simple.JSONArray();
		org.json.simple.JSONArray m = new org.json.simple.JSONArray();

		for (int i = 0; i < this.clubs.size(); i++){
			org.json.simple.JSONObject tmp = new org.json.simple.JSONObject();
			FootballClub club = this.clubs.get(i);
			tmp.put("name", club.getName());
			tmp.put("location", club.getLocation());
			c.add(tmp);
		}
		for (int i = 0; i < this.matches.size(); i++){
			org.json.simple.JSONObject tmp   = new org.json.simple.JSONObject();
			org.json.simple.JSONArray teams  = new org.json.simple.JSONArray();
			org.json.simple.JSONObject team1 = new org.json.simple.JSONObject();
			org.json.simple.JSONObject team2 = new org.json.simple.JSONObject();
			org.json.simple.JSONObject date  = new org.json.simple.JSONObject();
			FootballMatch match = this.matches.get(i);
			Hashtable<String, Integer> d = match.getDateHash();

			tmp.put("name-1", match.one.getName());
			tmp.put("goals-1", match.one.getScored());

			tmp.put("name-2", match.two.getName());
			tmp.put("goals-2", match.two.getScored());

			date.put("year", d.get("year"));
			date.put("month", d.get("month"));
			date.put("day", d.get("day"));

			tmp.put("date", date);

			m.add(tmp);
		}

		json.put("matches", m);
		json.put("clubs", c);

		try{
			FileWriter out = new FileWriter("data.json");
			out.write(org.json.simple.JSONObject.toJSONString(json));
			out.close();
		} catch (java.io.IOException e){
			System.out.println("ERROR: Error writing to file");
		}
	}

	private void _loadJSON(){ // Tutor requested JSON persistence
		if (! new java.io.File("data.json").exists()){
			System.out.println("ERROR: No save file present");
			return;
		}

		try{
			Object tmp = new org.json.simple.parser.JSONParser().parse(new java.io.FileReader("data.json"));
			org.json.simple.JSONObject decoded = (org.json.simple.JSONObject)tmp;

			this.clubs.clear();
			this.matches.clear();

			java.util.Iterator it = ((org.json.simple.JSONArray)decoded.get("clubs")).iterator();
			System.out.println("Clubs:");
			while (it.hasNext()){
				org.json.simple.JSONObject tmpA = (org.json.simple.JSONObject)it.next();
				System.out.println("    " + tmpA.toString());
				try{
					this.createClub(
						(String)tmpA.get("name"),
						(String)tmpA.get("location")
					);
				} catch(InvalidClubPropertyException e){
					System.out.println("ERROR: Invalid club properties found");
				}
			}

			it = ((org.json.simple.JSONArray)decoded.get("matches")).iterator();
			System.out.println("Matches:");
			while (it.hasNext()){
				org.json.simple.JSONObject tmpA = (org.json.simple.JSONObject)it.next();
				System.out.println("    " + tmpA.toString());

				GregorianCalendar c = new GregorianCalendar(
					((Long)((org.json.simple.JSONObject)tmpA.get("date")).get("year")).intValue(),
					((Long)((org.json.simple.JSONObject)tmpA.get("date")).get("month")).intValue() - 1,
					((Long)((org.json.simple.JSONObject)tmpA.get("date")).get("day")).intValue()
				);

				if (this._isFuture(c)){
					this.createMatch(
						c,
						(String)tmpA.get("name-1"),
						(String)tmpA.get("name-2")
					);
				} else{
					this.createMatch(
						c,
						(String)tmpA.get("name-1"),
						((Long)tmpA.get("goals-1")).intValue(),
						(String)tmpA.get("name-2"),
						((Long)tmpA.get("goals-2")).intValue()
					);
				}
			}
			System.out.println("Successfuly imported clubs and matches");
		} catch (org.json.simple.parser.ParseException e){
			System.out.println("ERROR: Parser exception");
			e.printStackTrace();
		} catch (java.io.FileNotFoundException e){
			System.out.println("ERROR: File not found exception");
		} catch (java.io.IOException e){
			System.out.println("ERROR: IO Exception");
		}
		
	}

	private void _sort(){
		Collections.sort(this.clubs, new Comparator<FootballClub>(){
			@Override
			public int compare(FootballClub c1, FootballClub c2){
				if (c1.getPoints() > c2.getPoints()){
					return -1;
				} else if (c1.getPoints() == c2.getPoints()){
					if ((c1.getGoalsScored() - c1.getGoalsReceived()) > (c2.getGoalsScored() - c2.getGoalsReceived())){
						return 1;
					} else if ((c1.getGoalsScored() - c1.getGoalsReceived()) == (c2.getGoalsScored() - c2.getGoalsReceived())){
						return 0;
					} else{
						return -1;
					}
				} else{
					return 1;
				}
			}
		});
	}

	private void _block(){
		System.out.println();
		System.out.print("Press [ENTER] to continue...");
		this.in.nextLine();
		System.out.println(); System.out.println();
	}
}
