package uow;

import uow.league.PremierLeagueManager;

public class Main{
	public static void main(String[] args){
		PremierLeagueManager plm = new PremierLeagueManager();
		while (plm.isRunning()){ plm.displayMenu(); }
	}
}
