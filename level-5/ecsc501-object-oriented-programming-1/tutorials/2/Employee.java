public class Employee{
	String name;
	double salary;

	public Employee(){
	//	this.name = "";
	//	this.salary = 0;
	}

	public Employee(String name, double salary){
		this.name = name;
		this.salary = salary;
	}


	public String getName(){
		return this.name;
	}

	public double getSalary(){
		return this.salary;
	}
}
