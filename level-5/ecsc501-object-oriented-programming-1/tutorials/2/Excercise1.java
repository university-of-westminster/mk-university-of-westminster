public class Excercise1{
	public static void main(String[] args){
		VendingMachine vm  = new VendingMachine();
		VendingMachine vmi = new VendingMachine(5);

		System.out.println("Default machine:     c:" + vm.getCanCount() + ", t:" + vm.getTokenCount());
		System.out.println("Initialized machine: c:" + vmi.getCanCount() + ", t:" + vmi.getTokenCount());

		vm.insertToken(); vm.insertToken();
		vmi.insertToken(); vmi.insertToken();

		System.out.println("Default machine:     c:" + vm.getCanCount() + ", t:" + vm.getTokenCount());
		System.out.println("Initialized machine: c:" + vmi.getCanCount() + ", t:" + vmi.getTokenCount());
	}
}
