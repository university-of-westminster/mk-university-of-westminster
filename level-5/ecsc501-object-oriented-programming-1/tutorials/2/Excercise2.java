public class Excercise2{
	public static void main(String[] args){
		Employee empt = new Employee();
		Employee fill = new Employee("John", 35750.75);

		System.out.println("Uninit: N:'" + empt.getName() + "', S: " + empt.getSalary());
		System.out.println("Init:   N:'" + fill.getName() + "', S: " + fill.getSalary());
	}
}
