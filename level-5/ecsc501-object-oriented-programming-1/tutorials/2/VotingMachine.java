public class VotingMachine{
	private final boolean BIASED       = true;
	public final int VOTE_LABOUR       = 0x01;
	public final int VOTE_CONSERVATIVE = 0x10;
	public int labourVotes;
	public int conservativeVotes;

	public VotingMachine(){
		this.labourVotes = 0;
		this.conservativeVotes = 0;
	}


	public void vote(int partyId){
		if (partyId == this.VOTE_LABOUR){
			if (this.BIASED){
				this.labourVotes += Math.round(Math.random()*10+1);
			} else{
				this.voteLabour();
			}
		} else if (partyId == this.VOTE_CONSERVATIVE){
			this.voteConservative();
		} else{
			System.out.println("Unrecognized party ID.");
		}
	}

	public void voteLabour(){
		System.out.println("Voting labour...");
		this.labourVotes++;
	}

	public void voteConservative(){
		System.out.println("Voting conservative...");
		this.conservativeVotes++;
	}


	public void displayStats(){
		System.out.println("Votes:");
		System.out.println("         Labour: " + this.getLabourVotes());
		System.out.println("   Conservative: " + this.getConservativeVotes());
	}

	public int getLabourVotes(){
		return this.labourVotes;
	}

	public int getConservativeVotes(){
		return this.conservativeVotes;
	}

	public void reset(){
		System.out.println("Resetting machine...");
		this.labourVotes = 0;
		this.conservativeVotes = 0;
	}
}
