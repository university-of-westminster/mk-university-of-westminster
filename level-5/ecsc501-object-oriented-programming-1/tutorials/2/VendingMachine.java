public class VendingMachine{
	public int cans;
	public int tokens;

	public VendingMachine(){
		this.cans = 10;
		this.tokens = 0;
	}

	public VendingMachine(int cans){
		this.cans = cans;
		this.tokens = 0;
	}


	public void fillUp(int cans){
		this.cans += cans;
	}

	public void insertToken(){
		System.out.println("Token inserted, dispensing can...");
		this.tokens++;
		this.cans--;
	}


	public int getCanCount(){
		return this.cans;
	}

	public int getTokenCount(){
		return this.tokens;
	}
}
