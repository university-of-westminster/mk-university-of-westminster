public class Excercise3{
	public static void main(String[] args){
		VotingMachine vm = new VotingMachine();
		vm.displayStats();
		vm.voteLabour();
		vm.voteConservative();
		vm.voteLabour();
		vm.voteConservative();
		vm.vote(vm.VOTE_LABOUR);
		vm.vote(vm.VOTE_CONSERVATIVE);
		vm.displayStats();
		System.out.println(vm.getLabourVotes() + " - " + vm.getConservativeVotes());
	}
}
