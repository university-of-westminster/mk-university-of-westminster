class Car {
    private String licensePlate; 
    private double speed;        // kilometers per hour
    private double maxSpeed;     // kilometers per hour
  
    // constructors
    public Car(String licensePlate1, double maxSpeed1) {
        this.licensePlate = licensePlate1; 
        this.speed  = 0.0;
        if (maxSpeed1 >= 0.0) {
            maxSpeed = maxSpeed1;
        }
        else {
            maxSpeed = 0.0;
        }
    }   

    /*********************************************/
    /* Code for overloaded constructor goes here */
    /*********************************************/
	public Car(String licensePlate, double maxSpeed, double speed1){
		this.licensePlate = licensePlate;

		if (speed1 < 0){ this.speed = 0.0; }
		else if (speed1 > maxSpeed){ this.speed = maxSpeed; }
		else { this.speed = speed1; }

		if (maxSpeed >= 0.0){ this.maxSpeed = maxSpeed; }
		else{ this.maxSpeed = 0.0; }
	}


	public void print(){
		System.out.println("Car properties:");
		System.out.println("   Current speed: " + this.speed);
		System.out.println("   Maximum speed: " + this.maxSpeed);
		System.out.println("   License plate: " + this.licensePlate);
	}
}

class CarTest{
	public Car one;
	public Car two;

	public CarTest(){
		this.one = new Car("ABC-CONSTR-1", 65.3);
		this.two = new Car("DEF-CONSTR-2", 80.6, 90.9);
	}

	public void test(){
		this.one.print();
		this.two.print();
	}
}

public class Excercise7{
	public static void main(String[] args){
		CarTest test = new CarTest();
		test.test();
	}
}
