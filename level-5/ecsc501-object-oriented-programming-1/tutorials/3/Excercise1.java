import java.util.Scanner;

public class Excercise1{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		//in.useDelimiter(System.getPropert("line.separator"));
		System.out.print("Enter your name: ");
		Greeter g = new Greeter(in.nextLine());

		String line;
		System.out.print("Query: ");
		line = in.nextLine();
		while (true){
			if (line.indexOf("please") != -1){
				System.out.println(g.sayHello());
			} else{
				System.out.println(g.refuseHelp());
			}
			System.out.print("Query: ");
			line = in.nextLine();
		}

	}
}
