public class Barcode{
	private final boolean NICE_FORMAT = false;
	private String barcode;
	private String decoded;

	private String[] barcodeRefTable = {
						"||:::",
						":::||",
						"::|:|",
						"::||:",
						":|::|",
						":|:|:",
						":||::",
						"|:::|",
						"|::|:",
						"|:|::"
					};

	public Barcode(String zip){
		this.decoded = zip;
		System.out.println(this.encode());
	}

	public Barcode(BarcodeEncoded barcode){
		this.barcode = barcode.str();
		System.out.println(this.decode());
	}


	public String encode(){
		if (this.decoded.length() != 5){
			System.out.println("ERROR: Invalid ZIP format!");
			return "ERROR";
		}
		System.out.println("Encoding " + this.decoded);
		if (decoded.length() != 5){
			this.barcode = "Invalid ZIP code";
			return this.barcode;
		}
		try{
			this.barcode = "|";
			int sum = 0;
			for (int i = 0; i < this.decoded.length(); i++){
				int digit = Integer.parseInt(Character.toString(this.decoded.charAt(i)));
				this.barcode += this.barcodeRefTable[digit];
				if (this.NICE_FORMAT){ this.barcode += " "; }
				sum += digit;
			}
			System.out.println("Sum is " + sum);
			this.barcode += this.barcodeRefTable[this.computeChecksum(sum)];
			this.barcode += "|";
		} catch (NumberFormatException e){
			System.out.println("ERROR: Invalid ZIP format!");
			return "ERROR";
		}
		return this.barcode;
	}

	public String decode(){
		if (this.barcode.length() != 32){
			System.out.println("ERROR: Invalid barcode format!");
			return "ERROR";
		}
		System.out.println("Decoding " + this.barcode);
		this.barcode = this.barcode.replace(" ", "");
		this.barcode = this.barcode.substring(1, this.barcode.length()-1);
		this.decoded = "";
		for (int i = 0; i < 6; i++){
			String refCode = this.barcode.substring(i*5, (i*5)+5);
			for (int j = 0; j < this.barcodeRefTable.length; j++){
				if (refCode.equals(this.barcodeRefTable[j])){
					this.decoded += Integer.toString(j);
				}
			}
		}
		this.checkDecodeIntegrity();
		return this.decoded;
	}

	private int computeChecksum(int sum){
		int result = sum % 10;
		if (result != 10){
			result = 10 - result;
		} else{
			result = 0;
		}
		return result;
	}

	private void checkDecodeIntegrity(){
		int sum = 0;
		for (int i = 0; i < 5; i++){
			sum += Integer.parseInt(Character.toString(this.decoded.charAt(i)));
		}
		int checksum = this.computeChecksum(sum);
		if (checksum == Integer.parseInt(Character.toString(this.decoded.charAt(5)))){
			System.out.println("Checksum matching, ZIP code decoded.");
			this.decoded = this.decoded.substring(0, this.decoded.length()-1);
		} else{
			System.out.println("ERROR: Integrity check failed");
		}
	}
}
