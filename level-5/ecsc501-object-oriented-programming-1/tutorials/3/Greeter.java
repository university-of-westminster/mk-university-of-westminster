public class Greeter{
	private String s;

	public Greeter(String str){
		this.s = str;
	}

	public Greeter(){
		this.s = "Nobody";
	}

	public String sayHello(){
		return "Hello, " + this.s + "!";
	}

	public String sayGoodbye(){
		return "Goodbye " + this.s + "!";
	}

	public String refuseHelp(){
		return "I am sorry " + this.s + ", I don't think I can do that.";
	}
}
