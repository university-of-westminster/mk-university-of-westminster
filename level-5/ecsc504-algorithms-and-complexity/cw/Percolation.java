/*************************************************************************
 *  Author: Dr E Kapetanios
 *  Last update: 17-02-2016
 *
 *************************************************************************/

/*************************************************************************
 *  Add here your answers in regards with the performance analysis
 *  ---------------------------------------------------------------
 *  
 *  For this coursework I have decided to implement 2 slightly different algorithms
 *  The core algorithm behind both of them is similar, however one uses recursion
 *  while the other one uses a queue
 *
 *  Having spent some time thinking about the percolation problem, I am quite sure
 *  the below algorithm cannot be further greatly optimized
 *  In it's current state it is using a brute-force technique, exhaustively travelling
 *  through the search space
 *  Considering the problem at hand, I find it impossible to solve without a
 *  (mostly) complete search of the input array as the data is completely
 *  random and pattern-less (thus we are unable to apply principles found in
 *  divide-and-conquer bisect for example)
 *
 *  Currently, the percolates() function uses the flow() function as it's algorithm,
 *  checking if there is an open site in the bottom row
 *  Granted, if we made a slight change to the algorithm specifically for the
 *  percolates() function to stop immediately after it encounters an open site
 *  in the bottom row, it should provide slightly better performance as it doesn't
 *  unnecessarily waste CPU cycles on discovering the route further (potentially
 *  taking longer for the extended route search than it took for the main objective)
 *
 *
 *  Performance analysis:
 *    All analysis has been done based on the output of the analyze.pl script
 *    You can find the full output at the end of this file (final times are at
 *    the very bottom)
 *
 *    flow():
 *      Recursive:
 *        A nearly constant ratio of 2 between runs suggests linear O(n) performance,
 *        this can be further confirmed by looking at the worst case scenario which
 *        would require approximately N/2 steps
 *        Big theta: O(n)
 *      Queued:
 *        Same performance in terms of scalability as in the recursive implementation,
 *        however generally taking twice as long as its recursive counterpart
 *        This can be attributed to the dynamic allocating and overhead of the
 *        queue implementation as opposed to raw arrays
 *      Direct:
 *        Our doubling hypothesis analysis suggests the performance being O(log n),
 *        having the ratio move between 1 and 1.5
 *        However performing code analysis we can argue that a worst case scenario could
 *        potentially require this algorithm to perform a search through nearly every
 *        single array element, thus making the complexity linear - O(n)
 *        Given the possible worst case scenario, I'd say that the big theta for this
 *        algorithm is in the O(n) space as well
 *
 *    percolates():
 *      Same performance as the flow() function for both recursive and queued implementation
 *      as they use the flow() function inside them
 *      Direct:
 *        Ratios jumped up to around 2 for this function, that is probably due to the fact
 *        that it doesnt terminate upon first reaching the bottom row, but rather continues
 *        to discover all possible vertical routes
 *        Complexity: O(n)
 *        Big theta: O(n)
 *
 *
 *
 *  Probability:
 *    Output:
 *      Generating 300 x 300 array with 0.3000 probability
 *      Probability     : 0.0 
 *      Generating 300 x 300 array with 0.4000 probability
 *      Probability     : 0.0 
 *      Generating 300 x 300 array with 0.5000 probability
 *      Probability     : 0.0 
 *      Generating 300 x 300 array with 0.5500 probability
 *      Probability     : 0.0 
 *      Generating 300 x 300 array with 0.5700 probability
 *      Probability     : 0.002
 *      Generating 300 x 300 array with 0.5800 probability
 *      Probability     : 0.045
 *      Generating 300 x 300 array with 0.5900 probability
 *      Probability     : 0.36
 *      Generating 300 x 300 array with 0.5910 probability
 *      Probability     : 0.415
 *      Generating 300 x 300 array with 0.5920 probability
 *      Probability     : 0.443
 *      Generating 300 x 300 array with 0.5935 probability
 *      Probability     : 0.483
 *      Generating 300 x 300 array with 0.5936 probability
 *      Probability     : 0.492
 *      Generating 300 x 300 array with 0.5937 probability
 *      Probability     : 0.511
 *    Comments:
 *      Based on the measured results (seen in the output above), we can conclude that the
 *      probability of a system percolating is absolutely minimal below p=0.57, however
 *      growing exponentially above this value
 *      By further examining the output we can say that the threshold probability (the
 *      probability above which percolation almost certainly occurs) is around the
 *      p=5937 value as it very quickly grows/shrinks above and below it
 *
 *    I am not sure why we are supposed to time / analyze this function. I have not timed
 *    minet, altough looking at the implementation it is dependent on both the percolates()
 *    function complexity as well as the amount of iterations needed to calculate the the
 *    probability, thus combining 2 linear O(n) in my case
 *  
 *
 *************************************************************************/

import java.util.LinkedList;

public class Percolation {

    public static enum Algo{ QUEUE, RECURSIVE, DIRECT }

    // given an N-by-N matrix of open sites, return an N-by-N matrix
    // of sites reachable from the top
    public static boolean[][] flow(boolean[][]open){ return Percolation.flow(open, Percolation.Algo.RECURSIVE); }

    public static boolean[][] flow(boolean[][] open, Percolation.Algo algo) {
	boolean[][] touched = new boolean[open.length][open[0].length];
        switch (algo){
		case DIRECT:
			for (int i = 0; i < open[0].length; i++){
				for (int j = 0; j < open.length; j++){
					if (open[j][i]){ touched[j][i] = true; }
					else{ break; }
				}
			}
			break;
		case QUEUE:
			Percolation.flow(open, touched);
			break;
		case RECURSIVE:
			for (int i = 0; i < open[0].length; i++){
				if (open[0][i]){ Percolation.flow(open, touched, i, 0); }
			}
			break;
	}

	return touched;
    }

    // Recursive flow
    public static void flow(boolean[][] open, boolean[][] touched, int x, int y){
	if (
		(x < 0) ||
		(y < 0) ||
		(x >= open[0].length) ||
		(y >= open.length)
	){ return; }

	if (touched[y][x] || !open[y][x]){ return; }

	touched[y][x] = true;
	Percolation.flow(open, touched, x + 1, y);
	Percolation.flow(open, touched, x - 1, y);
	Percolation.flow(open, touched, x, y + 1);
	Percolation.flow(open, touched, x, y - 1);
    }

    // Queued flow
    public static void flow(boolean[][] open, boolean[][] touched){
	LinkedList<Coord> queue = new LinkedList<Coord>();
	for (int i = 0; i < open[0].length; i++){
		if (open[0][i]){queue.add(new Coord(i, 0)); }
	}

	while (queue.size() > 0){
		Coord tmp = queue.pollFirst();
		if (tmp.y < (open.length - 1)){
			if (open[tmp.y + 1][tmp.x] && !touched[tmp.y + 1][tmp.x]){
				queue.add(new Coord(tmp.x, tmp.y + 1));
				touched[tmp.y + 1][tmp.x] = true;
			}
		}
		if (tmp.x > 0){
			if (open[tmp.y][tmp.x - 1] && !touched[tmp.y][tmp.x - 1]){
				queue.add(new Coord(tmp.x - 1, tmp.y));
				touched[tmp.y][tmp.x - 1] = true;
			}
		}
		if (tmp.x < (open[tmp.y].length - 1)){
			if (open[tmp.y][tmp.x + 1] && !touched[tmp.y][tmp.x + 1]){
				queue.add(new Coord(tmp.x + 1, tmp.y));
				touched[tmp.y][tmp.x + 1] = true;
			}
		}
		if (tmp.y > 0){
			if (open[tmp.y - 1][tmp.x] && !touched[tmp.y - 1][tmp.x]){
				queue.add(new Coord(tmp.x, tmp.y - 1));
				touched[tmp.y - 1][tmp.x] = true;
			}
		}
	}
    }




    // does the system percolate?
    public static boolean percolates(boolean[][] open, Percolation.Algo algo){
	boolean[][] touched = null;
	switch (algo){
		case DIRECT:
			return Percolation.percolatesDirect(open);
		case QUEUE:
			touched = Percolation.flow(open, Percolation.Algo.QUEUE);
			break;
		case RECURSIVE:
			touched = Percolation.flow(open, Percolation.Algo.RECURSIVE);
			break;
	}

	if (touched != null){
		for (int i = 0; i < touched[touched.length - 1].length; i++){
			if (touched[touched.length - 1][i]){ return true; }
		}
	}

	return false;
    }

    public static boolean percolates_queue(boolean[][] open) {
    	// To be completed for the course work...
	if (open.length < 1){ return false; }
	if (open[0].length < 1){ return false; }
	if (open.length == 1 && open[0].length == 1){
		return open[0][0];
	}
	boolean[][] touched = new boolean[open.length][open[0].length];
	LinkedList<Coord> queue = new LinkedList<Coord>();
	for (int i = 0; i < open[0].length; i++){
		if (open[0][i]){
			queue.add(new Coord(i, 0));
			touched[0][i] = true;
		}
	}
	while (queue.size() > 0){
		Coord tmp = queue.pollFirst();
		if (tmp.y < (open.length - 1)){
			if (open[tmp.y + 1][tmp.x] && !touched[tmp.y + 1][tmp.x]){
				if ((tmp.y + 1) == (open.length - 1)){ return true; }
				queue.add(new Coord(tmp.x, tmp.y + 1));
				touched[tmp.y + 1][tmp.x] = true;
			}
		}
		if (tmp.x > 0){
			if (open[tmp.y][tmp.x - 1] && !touched[tmp.y][tmp.x - 1]){
				queue.add(new Coord(tmp.x - 1, tmp.y));
				touched[tmp.y][tmp.x - 1] = true;
			}
		}
		if (tmp.x < (open[tmp.y].length - 1)){
			if (open[tmp.y][tmp.x + 1] && !touched[tmp.y][tmp.x + 1]){
				queue.add(new Coord(tmp.x + 1, tmp.y));
				touched[tmp.y][tmp.x + 1] = true;
			}
		}
		if (tmp.y > 0){
			if (open[tmp.y - 1][tmp.x] && !touched[tmp.y - 1][tmp.x]){
				queue.add(new Coord(tmp.x, tmp.y - 1));
				touched[tmp.y - 1][tmp.x] = true;
			}
		}
	}


        return false;
    }
    
 // does the system percolate vertically in a direct way?
    public static boolean percolatesDirect(boolean[][] open) {
	for (int i = 0; i < open[0].length; i++){
		if (open[0][i]){
			// Should we consider Nx1 matrix? We need to check here if yes - currently unsupported
			boolean percolates = true;
			for (int j = (open.length - 1); j > 0; j--){
				if (!open[j][i]){
					percolates = false;
					break;
				}
			}
			if (percolates){ return true; }
		}
	}
    	
        return false;
    }

    // draw the N-by-N boolean matrix to standard draw
    public static void show(boolean[][] a, boolean which) {

        int N = a.length;
        StdDraw.setXscale(-1, N);
        StdDraw.setYscale(-1, N);
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (a[i][j] == which)
                    StdDraw.filledSquare(j, N-i-1, .5);
    }
    
    // return a random N-by-N boolean matrix, where each entry is
    // true with probability p
    public static boolean[][] random(int N, double p) {
        boolean[][] a = new boolean[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                a[i][j] = StdRandom.bernoulli(p);
        return a;
    }
    
    public static double eval(int N, double p, int M, Percolation.Algo algo) {
	double probability = 0.0;
	for (int i = 0; i < M; i++){
		boolean[][] matrix = random(N, p);
		if (Percolation.percolates(matrix, algo)){ probability += 1.0; }
	}

	return (probability / M);
    }

    // test client
    public static void main(String[] args) {
    	
    	// A matrix 5-by-5 will be randomly generated
    	// The dimensions should be changed, while you are testing and during the viva, in order to analyse the performance of the algorithm

	int size = 5;
	double probability = 0.5;
    	
    	boolean[][] randomlyGenMatrix;
	if (args.length == 1){ size = Integer.parseInt(args[0]); }
	else if (args.length == 2){
		size = Integer.parseInt(args[0]);
		probability = Double.parseDouble(args[1]);
	}

	StdOut.println(String.format("Generating %1$d x %1$d array with %2$.3f probability", size, probability));
	randomlyGenMatrix = random(size, probability);
    	
    	//StdArrayIO.print(randomlyGenMatrix);
    	//show(randomlyGenMatrix, true);
        
	Stopwatch timer = null;
	boolean[][] percolatingMatrix;

	StdOut.println("Start...");

	//StdOut.println("Probability     : " + Percolation.eval(size, probability, 1000, Percolation.Algo.RECURSIVE));

	Percolation.percolates(randomlyGenMatrix, Percolation.Algo.RECURSIVE); // Very weird results without a dry run eg. flow() taking more time than percolates(). Caching?
	StdOut.println("================================");
	StdOut.println("RECURSIVE PERCOLATION:");
	timer = new Stopwatch();
	percolatingMatrix = flow(randomlyGenMatrix, Percolation.Algo.RECURSIVE);
	StdOut.println("Flow time       : rtf=" + timer.elapsedTime());
	//StdArrayIO.print(percolatingMatrix);
	timer = new Stopwatch();
	StdOut.println("Percolates      : " + percolates(randomlyGenMatrix, Percolation.Algo.RECURSIVE));
	StdOut.println("Percolation time: rtp=" + timer.elapsedTime());
	StdOut.println("================================");
        
	StdOut.println("================================");
	StdOut.println("QUEUED PERCOLATION:");
	Percolation.percolates(randomlyGenMatrix, Percolation.Algo.QUEUE); // Very weird results without a dry run eg. flow() taking more time than percolates(). Caching?
	timer = new Stopwatch();
	percolatingMatrix = flow(randomlyGenMatrix, Percolation.Algo.QUEUE);
	StdOut.println("Flow time       : qtf=" + timer.elapsedTime());
	//StdArrayIO.print(percolatingMatrix);
	timer = new Stopwatch();
	StdOut.println("Percolates      : " + percolates(randomlyGenMatrix, Percolation.Algo.QUEUE));
	StdOut.println("Percolation time: qtp=" + timer.elapsedTime());
	StdOut.println("================================");
	StdOut.println("================================");
	StdOut.println("DIRECT PERCOLATION:");
	Percolation.percolates(randomlyGenMatrix, Percolation.Algo.DIRECT); // Very weird results without a dry run eg. flow() taking more time than percolates(). Caching?
	timer = new Stopwatch();
	percolatingMatrix = flow(randomlyGenMatrix, Percolation.Algo.DIRECT);
	StdOut.println("Flow time       : dtf=" + timer.elapsedTime());
	//StdArrayIO.print(percolatingMatrix);
	timer = new Stopwatch();
	StdOut.println("Percolates      : " + percolates(randomlyGenMatrix, Percolation.Algo.DIRECT));
	StdOut.println("Percolation time: dtp=" + timer.elapsedTime());
	StdOut.println("================================");

    }

}

class Coord{
	public int x;
	public int y;

	public Coord(int x, int y){
		this.x = x;
		this.y = y;
	}
}

/*

./analyze.pl --iterations 50


Running analysis with 50 iterations and p=0.7 for every size
Analyzing grids 100*100 (10000 items)
..................................................
Analysis for grid 100*100 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.0005 (0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.001, 0.0, 0.001, 0.001, 0.001, 0.0, 0.001, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.001, 0.001, 0.0, 0.001, 0.001, 0.0, 0.001, 0.001, 0.001, 0.0, 0.001, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.001, 0.001, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0)
    -Flow: 0.00056 (0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.001, 0.001, 0.0, 0.001, 0.001, 0.001, 0.001, 0.0, 0.001, 0.001, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.001, 0.001, 0.0, 0.001, 0.0, 0.0, 0.001, 0.001, 0.001)
  -Queued:
    -Perc: 0.00394 (0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.004, 0.003, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.004, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.005, 0.004, 0.004, 0.004, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.004, 0.004, 0.003, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003)
    -Flow: 0.00438 (0.004, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.005, 0.004, 0.004, 0.004, 0.005, 0.004, 0.005, 0.004, 0.005, 0.004, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.004, 0.004, 0.004, 0.005, 0.004, 0.005, 0.005, 0.004, 0.004, 0.005, 0.004, 0.004, 0.004, 0.005, 0.004, 0.005, 0.005, 0.004, 0.003, 0.004, 0.004, 0.005, 0.005, 0.004)
  -Direct:
    -Perc: 4e-05 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    -Flow: 0 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 141*141 (19881 items)
..................................................
Analysis for grid 141*141 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.001 (0.0, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.002, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001)
    -Flow: 0.00106 (0.001, 0.001, 0.001, 0.001, 0.001, 0.002, 0.001, 0.001, 0.001, 0.001, 0.002, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.002, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001)
  -Queued:
    -Perc: 0.00720000000000001 (0.007, 0.007, 0.007, 0.008, 0.007, 0.008, 0.007, 0.008, 0.008, 0.007, 0.008, 0.007, 0.007, 0.008, 0.007, 0.007, 0.008, 0.007, 0.007, 0.008, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.008, 0.007, 0.007, 0.006, 0.008, 0.007, 0.007, 0.007, 0.008, 0.007, 0.006, 0.007, 0.008, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007)
    -Flow: 0.00794 (0.008, 0.008, 0.008, 0.009, 0.008, 0.009, 0.008, 0.008, 0.008, 0.008, 0.009, 0.008, 0.008, 0.008, 0.008, 0.007, 0.009, 0.007, 0.008, 0.009, 0.007, 0.009, 0.008, 0.008, 0.007, 0.007, 0.008, 0.007, 0.008, 0.008, 0.008, 0.007, 0.007, 0.008, 0.008, 0.007, 0.008, 0.007, 0.008, 0.008, 0.009, 0.008, 0.009, 0.008, 0.008, 0.008, 0.008, 0.007, 0.008, 0.008)
  -Direct:
    -Perc: 2e-05 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    -Flow: 0 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 200*200 (40000 items)
..................................................
Analysis for grid 200*200 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.00198 (0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.001, 0.002, 0.002, 0.001, 0.002, 0.002, 0.002, 0.004, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.003, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.001, 0.002, 0.002, 0.002, 0.002, 0.001, 0.002, 0.002, 0.002, 0.002)
    -Flow: 0.00202 (0.003, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.004, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.001, 0.002, 0.002, 0.002, 0.002, 0.001, 0.002, 0.002, 0.002, 0.002)
  -Queued:
    -Perc: 0.00628 (0.007, 0.007, 0.007, 0.006, 0.006, 0.006, 0.007, 0.006, 0.006, 0.006, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.008, 0.005, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.007, 0.006, 0.006, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.008, 0.008, 0.006, 0.006, 0.006, 0.006, 0.007, 0.006, 0.006, 0.006, 0.006)
    -Flow: 0.01532 (0.016, 0.015, 0.017, 0.014, 0.015, 0.015, 0.017, 0.015, 0.016, 0.014, 0.015, 0.015, 0.015, 0.015, 0.014, 0.017, 0.015, 0.014, 0.015, 0.015, 0.015, 0.014, 0.016, 0.016, 0.015, 0.015, 0.016, 0.015, 0.015, 0.017, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.016, 0.016, 0.016, 0.017, 0.014, 0.016, 0.015, 0.016, 0.015, 0.015, 0.016, 0.015, 0.016, 0.015)
  -Direct:
    -Perc: 0.00012 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0)
    -Flow: 0 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 283*283 (80089 items)
..................................................
Analysis for grid 283*283 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.00588 (0.006, 0.006, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.007, 0.006, 0.005, 0.004, 0.004, 0.006, 0.006, 0.006, 0.006, 0.007, 0.007, 0.006, 0.006, 0.005, 0.007, 0.006, 0.004, 0.005, 0.004, 0.006, 0.006, 0.006, 0.005, 0.006, 0.005, 0.007, 0.006, 0.006, 0.007, 0.006, 0.004, 0.006, 0.007, 0.008, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.005)
    -Flow: 0.00388 (0.004, 0.004, 0.004, 0.003, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.007, 0.004, 0.004, 0.004, 0.004, 0.004, 0.005, 0.004, 0.004, 0.003, 0.004, 0.004, 0.004, 0.003, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.004, 0.003, 0.004, 0.004, 0.004, 0.003, 0.003, 0.004, 0.004, 0.004, 0.004, 0.003, 0.004, 0.004, 0.003, 0.003, 0.004, 0.004)
  -Queued:
    -Perc: 0.00662 (0.006, 0.007, 0.009, 0.005, 0.007, 0.005, 0.005, 0.008, 0.008, 0.007, 0.005, 0.008, 0.008, 0.006, 0.005, 0.007, 0.008, 0.005, 0.007, 0.006, 0.009, 0.008, 0.006, 0.005, 0.007, 0.008, 0.006, 0.007, 0.005, 0.007, 0.005, 0.005, 0.008, 0.005, 0.005, 0.008, 0.005, 0.005, 0.008, 0.005, 0.007, 0.009, 0.009, 0.005, 0.008, 0.008, 0.008, 0.006, 0.008, 0.004)
    -Flow: 0.0098 (0.009, 0.009, 0.01, 0.01, 0.009, 0.01, 0.01, 0.009, 0.009, 0.01, 0.01, 0.01, 0.009, 0.01, 0.01, 0.009, 0.011, 0.01, 0.011, 0.009, 0.01, 0.01, 0.009, 0.009, 0.01, 0.016, 0.01, 0.01, 0.009, 0.009, 0.009, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.011, 0.01, 0.01, 0.009, 0.009, 0.009, 0.009, 0.01, 0.01, 0.01, 0.009, 0.01, 0.009)
  -Direct:
    -Perc: 0.00012 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0)
    -Flow: 8e-05 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 400*400 (160000 items)
..................................................
Analysis for grid 400*400 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.0046 (0.004, 0.005, 0.004, 0.005, 0.005, 0.005, 0.005, 0.005, 0.004, 0.004, 0.005, 0.005, 0.005, 0.004, 0.005, 0.005, 0.005, 0.004, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.005, 0.004, 0.005, 0.004, 0.005, 0.005, 0.005, 0.005, 0.004, 0.004, 0.005, 0.004, 0.004, 0.005, 0.005, 0.004, 0.004, 0.006, 0.004, 0.005, 0.006, 0.005, 0.005, 0.005, 0.004)
    -Flow: 0.00874 (0.009, 0.008, 0.008, 0.011, 0.008, 0.008, 0.012, 0.008, 0.012, 0.007, 0.009, 0.008, 0.008, 0.012, 0.008, 0.008, 0.008, 0.008, 0.01, 0.008, 0.007, 0.008, 0.009, 0.008, 0.008, 0.008, 0.012, 0.008, 0.008, 0.008, 0.011, 0.008, 0.008, 0.008, 0.012, 0.012, 0.008, 0.008, 0.008, 0.008, 0.01, 0.008, 0.009, 0.008, 0.008, 0.009, 0.008, 0.008, 0.008, 0.008)
  -Queued:
    -Perc: 0.01328 (0.013, 0.012, 0.012, 0.013, 0.012, 0.012, 0.011, 0.015, 0.013, 0.012, 0.013, 0.015, 0.015, 0.013, 0.015, 0.015, 0.013, 0.017, 0.015, 0.013, 0.013, 0.013, 0.012, 0.015, 0.015, 0.015, 0.012, 0.015, 0.016, 0.01, 0.013, 0.012, 0.009, 0.016, 0.013, 0.013, 0.013, 0.016, 0.013, 0.015, 0.01, 0.011, 0.015, 0.015, 0.01, 0.01, 0.015, 0.01, 0.015, 0.015)
    -Flow: 0.0131 (0.011, 0.011, 0.012, 0.011, 0.012, 0.012, 0.012, 0.016, 0.011, 0.012, 0.012, 0.016, 0.016, 0.011, 0.016, 0.016, 0.012, 0.016, 0.016, 0.012, 0.011, 0.012, 0.011, 0.016, 0.016, 0.016, 0.012, 0.015, 0.015, 0.01, 0.012, 0.012, 0.011, 0.016, 0.011, 0.011, 0.012, 0.015, 0.011, 0.016, 0.011, 0.011, 0.016, 0.015, 0.01, 0.011, 0.015, 0.011, 0.016, 0.015)
  -Direct:
    -Perc: 0.00014 (0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001)
    -Flow: 6e-05 (0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 566*566 (320356 items)
..................................................
Analysis for grid 566*566 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.00832 (0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.009, 0.009, 0.008, 0.008, 0.008, 0.008, 0.014, 0.008, 0.008, 0.008, 0.009, 0.008, 0.009, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.009, 0.009, 0.009, 0.009, 0.008, 0.008, 0.008, 0.009, 0.008, 0.008, 0.008, 0.009, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008)
    -Flow: 0.01014 (0.008, 0.009, 0.01, 0.009, 0.014, 0.008, 0.01, 0.013, 0.011, 0.008, 0.01, 0.008, 0.012, 0.013, 0.009, 0.014, 0.009, 0.012, 0.008, 0.009, 0.013, 0.009, 0.011, 0.012, 0.013, 0.008, 0.01, 0.01, 0.008, 0.012, 0.009, 0.01, 0.009, 0.01, 0.01, 0.012, 0.008, 0.008, 0.016, 0.009, 0.009, 0.009, 0.009, 0.014, 0.008, 0.008, 0.008, 0.013, 0.008, 0.01)
  -Queued:
    -Perc: 0.0238 (0.2, 0.026, 0.023, 0.026, 0.021, 0.022, 0.026, 0.02, 0.019, 0.022, 0.026, 0.026, 0.019, 0.026, 0.025, 0.02, 0.019, 0.019, 0.021, 0.023, 0.019, 0.027, 0.026, 0.023, 0.025, 0.022, 0.026, 0.023, 0.026, 0.026, 0.026, 0.026, 0.025, 0.026, 0.027, 0.026)
    -Flow: 0.0254 (0.026, 0.026, 0.027, 0.027, 0.028, 0.026, 0.021, 0.026, 0.025, 0.022, 0.028, 0.027, 0.027, 0.026, 0.024, 0.028, 0.026, 0.027, 0.025, 0.025, 0.028, 0.023, 0.022, 0.024, 0.026, 0.026, 0.021, 0.027, 0.028, 0.021, 0.021, 0.022, 0.024, 0.026, 0.022, 0.025, 0.026, 0.025, 0.028, 0.025, 0.026, 0.025, 0.026, 0.026, 0.026, 0.027, 0.027, 0.028, 0.025, 0.027)
  -Direct:
    -Perc: 8e-05 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0)
    -Flow: 0.00018 (0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 800*800 (640000 items)
..................................................
Analysis for grid 800*800 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.01628 (0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.017, 0.017, 0.017, 0.017, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.015, 0.017, 0.017, 0.017, 0.015, 0.016, 0.016, 0.016, 0.016, 0.017, 0.016, 0.017, 0.017, 0.017, 0.017, 0.016, 0.016, 0.017, 0.016, 0.016, 0.016, 0.016, 0.016, 0.017, 0.016, 0.016, 0.017, 0.017, 0.016, 0.016)
    -Flow: 0.01648 (0.016, 0.016, 0.016, 0.016, 0.017, 0.016, 0.018, 0.016, 0.016, 0.016, 0.015, 0.019, 0.015, 0.016, 0.016, 0.017, 0.017, 0.016, 0.016, 0.016, 0.015, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.017, 0.016, 0.016, 0.017, 0.02, 0.016, 0.016, 0.016, 0.016, 0.019, 0.016, 0.016, 0.016, 0.017, 0.02, 0.016, 0.016, 0.016, 0.016, 0.016, 0.018, 0.017, 0.018)
  -Queued:
    -Perc: 0.03262 (0.035, 0.03, 0.03, 0.031, 0.03, 0.035, 0.034, 0.035, 0.035, 0.029, 0.03, 0.03, 0.03, 0.03, 0.035, 0.037, 0.03, 0.035, 0.03, 0.031, 0.03, 0.034, 0.03, 0.035, 0.034, 0.03, 0.03, 0.03, 0.034, 0.03, 0.034, 0.035, 0.03, 0.03, 0.03, 0.035, 0.03, 0.035, 0.035, 0.03, 0.035, 0.035, 0.035, 0.035, 0.034, 0.035, 0.034, 0.035, 0.035, 0.035)
    -Flow: 0.04214 (0.043, 0.036, 0.036, 0.037, 0.036, 0.043, 0.044, 0.043, 0.043, 0.036, 0.036, 0.037, 0.045, 0.045, 0.043, 0.043, 0.045, 0.043, 0.044, 0.044, 0.045, 0.052, 0.045, 0.043, 0.043, 0.036, 0.044, 0.044, 0.044, 0.044, 0.043, 0.043, 0.036, 0.045, 0.036, 0.043, 0.045, 0.042, 0.043, 0.037, 0.043, 0.043, 0.043, 0.044, 0.043, 0.044, 0.043, 0.043, 0.043, 0.043)
  -Direct:
    -Perc: 0.00012 (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0)
    -Flow: 0.00022 (0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.001, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 1131*1131 (1279161 items)
..................................................
Analysis for grid 1131*1131 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.0319 (0.032, 0.032, 0.033, 0.031, 0.032, 0.033, 0.031, 0.032, 0.031, 0.033, 0.031, 0.033, 0.033, 0.033, 0.032, 0.033, 0.031, 0.032, 0.033, 0.032, 0.033, 0.033, 0.031, 0.031, 0.032, 0.031, 0.033, 0.031, 0.033, 0.031, 0.032, 0.033, 0.033, 0.031, 0.033, 0.031, 0.031, 0.031, 0.031, 0.031, 0.032, 0.032, 0.032, 0.031, 0.032, 0.032, 0.031, 0.032, 0.031, 0.031)
    -Flow: 0.03172 (0.031, 0.031, 0.034, 0.031, 0.031, 0.033, 0.032, 0.031, 0.032, 0.033, 0.031, 0.032, 0.033, 0.033, 0.031, 0.032, 0.032, 0.031, 0.033, 0.031, 0.033, 0.033, 0.031, 0.031, 0.031, 0.031, 0.032, 0.031, 0.032, 0.031, 0.031, 0.032, 0.033, 0.031, 0.033, 0.031, 0.031, 0.032, 0.032, 0.031, 0.033, 0.031, 0.031, 0.031, 0.031, 0.033, 0.031, 0.031, 0.031, 0.032)
  -Queued:
    -Perc: 0.0556 (0.055, 0.055, 0.055, 0.054, 0.062, 0.054, 0.054, 0.054, 0.054, 0.055, 0.054, 0.054, 0.054, 0.055, 0.055, 0.054, 0.055, 0.055, 0.054, 0.054, 0.054, 0.055, 0.055, 0.055, 0.054, 0.055, 0.065, 0.054, 0.055, 0.054, 0.056, 0.054, 0.057, 0.055, 0.054, 0.054, 0.055, 0.055, 0.054, 0.056, 0.054, 0.054, 0.054, 0.054, 0.054, 0.054, 0.055, 0.055, 0.055, 0.089)
    -Flow: 0.07372 (0.077, 0.065, 0.066, 0.076, 0.102, 0.082, 0.077, 0.074, 0.075, 0.077, 0.066, 0.067, 0.073, 0.065, 0.076, 0.077, 0.066, 0.073, 0.065, 0.066, 0.074, 0.064, 0.079, 0.066, 0.066, 0.077, 0.074, 0.07, 0.078, 0.077, 0.076, 0.076, 0.078, 0.074, 0.07, 0.078, 0.077, 0.077, 0.077, 0.074, 0.077, 0.075, 0.066, 0.077, 0.065, 0.067, 0.081, 0.084, 0.073, 0.074)
  -Direct:
    -Perc: 0.0002 (0.001, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.001)
    -Flow: 0.00046 (0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.001, 0.001, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.0, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.0, 0.001, 0.001, 0.001, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0)
---------------------------------------------------------------------------
Analyzing grids 1600*1600 (2560000 items)
..................................................
Analysis for grid 1600*1600 with p=0.7 after 50 iterations:
  -Recursive:
    -Perc: 0.06374 (0.063, 0.063, 0.063, 0.066, 0.066, 0.063, 0.066, 0.062, 0.063, 0.066, 0.063, 0.063, 0.063, 0.067, 0.063, 0.063, 0.062, 0.064, 0.065, 0.063, 0.064, 0.063, 0.063, 0.064, 0.064, 0.063, 0.063, 0.063, 0.063, 0.066, 0.063, 0.063, 0.066, 0.063, 0.063, 0.062, 0.062, 0.063, 0.063, 0.062, 0.064, 0.066, 0.063, 0.063, 0.066, 0.066, 0.064, 0.063, 0.063, 0.066)
    -Flow: 0.06432 (0.064, 0.063, 0.062, 0.066, 0.065, 0.065, 0.065, 0.063, 0.063, 0.066, 0.063, 0.063, 0.063, 0.09, 0.064, 0.063, 0.063, 0.064, 0.065, 0.063, 0.064, 0.063, 0.064, 0.063, 0.063, 0.063, 0.063, 0.064, 0.063, 0.065, 0.064, 0.064, 0.066, 0.063, 0.062, 0.063, 0.063, 0.065, 0.063, 0.063, 0.064, 0.066, 0.063, 0.062, 0.066, 0.066, 0.063, 0.063, 0.063, 0.066)
  -Queued:
    -Perc: 0.10926 (0.109, 0.109, 0.109, 0.112, 0.116, 0.109, 0.109, 0.11, 0.109, 0.11, 0.109, 0.109, 0.109, 0.11, 0.109, 0.109, 0.109, 0.108, 0.109, 0.109, 0.109, 0.11, 0.109, 0.109, 0.109, 0.109, 0.109, 0.11, 0.108, 0.108, 0.111, 0.109, 0.108, 0.109, 0.11, 0.108, 0.11, 0.109, 0.108, 0.109, 0.109, 0.109, 0.108, 0.109, 0.109, 0.11, 0.108, 0.11, 0.109, 0.109)
    -Flow: 0.13122 (0.135, 0.135, 0.132, 0.129, 0.157, 0.135, 0.123, 0.131, 0.125, 0.124, 0.134, 0.135, 0.135, 0.124, 0.132, 0.133, 0.131, 0.133, 0.123, 0.124, 0.122, 0.133, 0.132, 0.134, 0.136, 0.129, 0.132, 0.134, 0.134, 0.132, 0.123, 0.135, 0.135, 0.141, 0.131, 0.133, 0.132, 0.124, 0.124, 0.131, 0.135, 0.135, 0.122, 0.125, 0.123, 0.133, 0.132, 0.134, 0.13, 0.135)
  -Direct:
    -Perc: 0.00022 (0.0, 0.001, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.001, 0.0, 0.0, 0.0, 0.001, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.001, 0.001, 0.0, 0.0)
    -Flow: 0.001 (0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001)
---------------------------------------------------------------------------
Input - TimeRP   Ratio   |  TimeRF   Ratio   |  TimeQP   Ratio   |  TimeQF   Ratio   |  TimeDP   Ratio   |  TimeDF   Ratio 
1600  - 0.06374 [1.998]  |  0.06432 [2.028]  |  0.10926 [1.965]  |  0.13122 [1.780]  |  0.00022 [1.100]  |  0.00100 [2.174]
1131  - 0.03190 [1.959]  |  0.03172 [1.925]  |  0.05560 [1.704]  |  0.07372 [1.749]  |  0.00020 [1.667]  |  0.00046 [2.091]
800   - 0.01628 [1.957]  |  0.01648 [1.625]  |  0.03262 [1.371]  |  0.04214 [1.659]  |  0.00012 [1.500]  |  0.00022 [1.222]
566   - 0.00832 [1.809]  |  0.01014 [1.160]  |  0.02380 [1.792]  |  0.02540 [1.939]  |  0.00008 [0.571]  |  0.00018 [3.000]
400   - 0.00460 [0.782]  |  0.00874 [2.253]  |  0.01328 [2.006]  |  0.01310 [1.337]  |  0.00014 [1.167]  |  0.00006 [0.750]
283   - 0.00588 [2.970]  |  0.00388 [1.921]  |  0.00662 [1.054]  |  0.00980 [0.640]  |  0.00012 [1.000]  |  0.00008 [ -.--]
200   - 0.00198 [1.980]  |  0.00202 [1.906]  |  0.00628 [0.872]  |  0.01532 [1.929]  |  0.00012 [6.000]  |  0.00000 [ -.--]
141   - 0.00100 [2.000]  |  0.00106 [1.893]  |  0.00720 [1.827]  |  0.00794 [1.813]  |  0.00002 [0.500]  |  0.00000 [ -.--]
100   - 0.00050 [ -.--]  |  0.00056 [ -.--]  |  0.00394 [ -.--]  |  0.00438 [ -.--]  |  0.00004 [ -.--]  |  0.00000 [ -.--]


Finished in 491 seconds (8.18 minutes)




*/
