#!/usr/bin/env perl

use Getopt::Long;

#my @sizes = (5000, 7000, 10000, 14000, 20000); # approximate doubling
my @sizes = (100, 141, 200, 283, 400, 566, 800, 1131, 1600);
my $iterations = 10;
my $probability = 0.7;

GetOptions(
	'iterations=i' => \$iterations,
	'probability' => \$probability
);

my $time = time();

print("Running analysis with $iterations iterations and p=$probability for every size\n");

my %recursivePercolationResult;
my %recursiveFlowResult;
my %queuedPercolationResult;
my %queuedFlowResult;
my %directPercolationResult;
my %directFlowResult;

foreach (@sizes){
	my $recursivePercolation = 0.0;
	my @recursivePercolation;
	my $queuedPercolation    = 0.0;
	my @queuedPercolation;
	my $directPercolation    = 0.0;
	my @directPercolation;
	my $recursiveFlow        = 0.0;
	my @recursiveFlow;
	my $queuedFlow           = 0.0;
	my @queuedFlow;
	my $directFlow           = 0.0;
	my @directFlow;

	print "Analyzing grids $_*$_ (@{[$_*$_]} items)\n";

	for (my $i = 0; $i < $iterations; $i++){
		print ".";
		$result = `./run $_ $probability`;
		if ($result =~ m~.*rtf=([0-9]+[.]?[0-9]*).*rtp=([0-9]+[.]?[0-9]*).*qtf=([0-9]+[.]?[0-9]*).*qtp=([0-9]+[.]?[0-9]*).*dtf=([0-9]+[.]?[0-9]*).*dtp=([0-9]+[.]?[0-9]*)~is){
			push(@recursiveFlow, $1);
			$recursiveFlow += $1;
			push(@recursivePercolation, $2);
			$recursivePercolation += $2;
			push(@queuedFlow, $3);
			$queuedFlow += $3;
			push(@queuedPercolation, $4);
			$queuedPercolation += $4;
			push(@directFlow, $5);
			$directFlow += $5;
			push(@directPercolation, $6);
			$directPercolation += $6;
		}

		#$recursivePercolation += $1 if ($result =~ m~rtp=([0-9]+[.]?[0-9]*)~);
		#$queuedPercolation    += $1 if ($result =~ m~qtp=([0-9]+[.]?[0-9]*)~);
		#$directPeroclation    += $1 if ($result =~ m~dtp=([0-9]+[.]?[0-9]*)~);
		#$recursiveFlow += $1 if ($result =~ m~rtf=([0-9]+[.]?[0-9]*)~);
		#$queuedFlow    += $1 if ($result =~ m~qtf=([0-9]+[.]?[0-9]*)~);
		#$directFlow    += $1 if ($result =~ m~dtf=([0-9]+[.]?[0-9]*)~);
	}
	print "\n";

	$recursivePercolation = ($recursivePercolation / $iterations);
	$queuedPercolation    = ($queuedPercolation / $iterations);
	$directPercolation    = ($directPercolation / $iterations);
	$recursiveFlow = ($recursiveFlow / $iterations);
	$queuedFlow    = ($queuedFlow / $iterations);
	$directFlow    = ($directFlow / $iterations);

	$recursivePercolationResult{$_} = $recursivePercolation;
	$recursiveFlowResult{$_} = $recursiveFlow;
	$queuedPercolationResult{$_} = $queuedPercolation;
	$queuedFlowResult{$_} = $queuedFlow;
	$directPercolationResult{$_} = $directPercolation;
	$directFlowResult{$_} = $directFlow;

	print "Analysis for grid $_*$_ with p=$probability after $iterations iterations:\n";
	print "  -Recursive:\n";
	print "    -Perc: $recursivePercolation ("; print join(", ", grep{$_} @recursivePercolation); print ")\n";
	print "    -Flow: $recursiveFlow ("; print join(", ", grep{$_} @recursiveFlow); print ")\n";
	print "  -Queued:\n";
	print "    -Perc: $queuedPercolation ("; print join(", ", grep{$_} @queuedPercolation); print ")\n";
	print "    -Flow: $queuedFlow ("; print join(", ", grep{$_} @queuedFlow); print ")\n";
	print "  -Direct:\n";
	print "    -Perc: $directPercolation ("; print join(", ", grep{$_} @directPercolation); print ")\n";
	print "    -Flow: $directFlow ("; print join(", ", grep{$_} @directFlow); print ")\n";
	print "---------------------------------------------------------------------------\n";
}

printf("%-5s - %-7s %7s  |  %-7s %7s  |  %-7s %7s  |  %-7s %7s  |  %-7s %7s  |  %-7s %7s\n", "Input", "TimeRP", " Ratio ", "TimeRF", " Ratio ", "TimeQP", " Ratio ", "TimeQF", " Ratio ", "TimeDP", " Ratio ", "TimeDF", " Ratio ");
foreach $i (reverse(0 .. $#sizes)){
	printf(
		"%-5s - %.5f [%5s]  |  %.5f [%5s]  |  %.5f [%5s]  |  %.5f [%5s]  |  %.5f [%5s]  |  %.5f [%5s]\n",
		@sizes[$i],
		$recursivePercolationResult{@sizes[$i]},
		($i > 0 && $recursivePercolationResult{@sizes[$i-1]} > 0)?(sprintf("%.3f", ($recursivePercolationResult{@sizes[$i]} / $recursivePercolationResult{@sizes[$i-1]}))):("-.--"),
		$recursiveFlowResult{@sizes[$i]},
		($i > 0 && $recursiveFlowResult{@sizes[$i-1]} > 0)?(sprintf("%.3f", ($recursiveFlowResult{@sizes[$i]} / $recursiveFlowResult{@sizes[$i-1]}))):("-.--"),
		$queuedPercolationResult{@sizes[$i]},
		($i > 0 && $queuedPercolationResult{@sizes[$i-1]} > 0)?(sprintf("%.3f", ($queuedPercolationResult{@sizes[$i]} / $queuedPercolationResult{@sizes[$i-1]}))):("-.--"),
		$queuedFlowResult{@sizes[$i]},
		($i > 0 && $queuedFlowResult{@sizes[$i-1]} > 0)?(sprintf("%.3f", ($queuedFlowResult{@sizes[$i]} / $queuedFlowResult{@sizes[$i-1]}))):("-.--"),
		$directPercolationResult{@sizes[$i]},
		($i > 0 && $directPercolationResult{@sizes[$i-1]} > 0)?(sprintf("%.3f", ($directPercolationResult{@sizes[$i]} / $directPercolationResult{@sizes[$i-1]}))):("-.--"),
		$directFlowResult{@sizes[$i]},
		($i > 0 && $directFlowResult{@sizes[$i-1]} > 0)?(sprintf("%.3f", ($directFlowResult{@sizes[$i]} / $directFlowResult{@sizes[$i-1]}))):("-.--"),
	);
}

print "\n\n";
printf("Finished in %d seconds (%.2f minutes)\n", time() - $time, (time() - $time)/60.0);
