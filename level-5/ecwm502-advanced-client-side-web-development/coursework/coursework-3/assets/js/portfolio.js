$(function(){
	$('#menu').pin();
	$('.accordion').accordion({
		collapsible: true,
		heightStyle: 'content'
	});
	$('.mg-popup').magnificPopup({
		delegate: 'a',
		type: 'image',
		zoom:{
			enabled: true
		}
	});
	

	$('[id^=anchor-]').each(function(index, element){
		var e = $(element);
		$('#menu ul').append('<li><a href="#' + e.attr('id') + '">' + e.children('h3').text() + '</a></li>');
	});

	$('h1').textillate({
		in: {
			effect: 'rollIn',
			shuffle: true
		}
	});
	$('h1').on('inAnimationEnd.tlt', function(){
		$('.page').addClass('animated zoomIn');
		$('.page').removeClass('hidden');
		window.setTimeout(function(){
			$('.page').removeClass('animated zoomIn');
		}, 2000);
	});
});
