var highId = 1;

function addItem(title, description, price){
	$('.products').last().append(
		  '<div class="col-md-3 product" data-id="' + highId + '" data-price="' + price + '"><div class="panel panel-default"><div class="panel-heading"><span class="title">'
		+ title
		+ '</span><span class="label label-info pull-right price">'
		+ '$' + price
		+ '</span></div><div class="panel-body">'
		+ '<img data-src="holder.js/150x100">'
		+ description
		+ '</div></div></div>'
	);/*
	if ((highId % 4) == 0){
		$('.products-container').append('<div class="row products"></div>');
	}*/
	highId++;
}

function addBasket(b, id){
	$(b).find('.list-group').append(
		'<a class="list-group-item" data-id="' + id + '">'
		+ '<h4 class="list-group-item-heading">'
		+ $('.products').find('div.product[data-id="' + id + '"]').find('.title').text() + ' '
		+ '<span class="pull-right">' + $('.products').find('div.product[data-id="' + id + '"]').find('.price').text() + '</span></h4>'
		+ '<p class="list-group-item-text">Items: <button class="btn btn-sm btn-default more">+</button><input type="text" value="1" class="count"><button class="btn btn-sm btn-default less">-</button><button class="btn btn-sm btn-danger remove"><i class="glyphicon glyphicon-trash"></i></button></p></a>'
	);

	var element = $(b).find('.list-group').find('a[data-id="' + id + '"]');
	element.find('button.more').on('click', function(){
		var count = parseInt(element.find('.count').val());
		element.find('.count').val(count + 1);
		calculatePrice();
	});
	element.find('button.less').on('click', function(){
		var count = element.find('.count').val();
		if (count < 2){
			element.remove();
		} else{
			element.find('.count').val(count - 1);
		}
		calculatePrice();
	});
	element.find('button.remove').on('click', function(){
		element.remove();
		calculatePrice();
	});
}

function calculatePrice(){
	var result = 0;
	$('.list-group a').each(function(i, e){
		result += parseInt($(e).find('.count').val())*parseInt($(e).find('.pull-right').text().replace('$', ''));
	});
	if (result == 0){
		$('#total').text('No items in basket.');
	}else {
		$('#total').text('Total price: $' + result);
	}
}

$(function(){
	addItem('Lamp', 'Brings light into your room', 8);
	addItem('Monitor', 'Displays stuff', 89);
	addItem('Computer', 'Does what you tell it to do', 359);
	addItem('Piano', 'Plays sounds', 240);
	addItem('Chair', 'For sitting', 23);
	addItem('Table', 'Stores stuff on top of it', 40);

	$('.product').draggable({
		revert: true,
		drag: function(){
		}
	});
	$('.basket').droppable({
		hoverClass: 'panel-warning',
		tolerance: 'touch',
		drop: function(event, ui){
			var basket = $(this);
			var move   = ui.draggable;
			var item   = basket.find('.list-group a[data-id="' + move.attr('data-id') + '"]');

			if (item.html() != null){
				item.find('.count').val(parseInt(item.find('.count').val()) + 1);
			} else{
				addBasket(this, move.attr('data-id'));
			}
			calculatePrice();
		}
	});
});
