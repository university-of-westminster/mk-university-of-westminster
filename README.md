# University of Westminster
Level 4/5/6 UOW assessments.

Java / HTML / SQL

## Modules
### Level 4
* ECSC405 - Software Development Principles 2
* ECSC411 - Information Systems

### Level 5
* ECSC501 - Object Oriented Programming 1
* ECSC503 - Software Development Group Project
* ECSC505 - Professional Practice
* ECWM502 - Advanced Client-Side Web Development
