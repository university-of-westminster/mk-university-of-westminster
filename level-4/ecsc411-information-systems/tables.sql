drop table choir cascade constraints;
create table choir (
cid		varchar2(3),
cname		varchar2(20),
ctype		varchar2(10),
csize		number(2));

drop table music cascade constraints;
create table music (
composer	varchar2(10),
work		varchar2(30),
accompaniment	varchar2(15));

drop table venue cascade constraints;
create table venue (
vnum		number(1),
location	varchar2(15),
capacity	number(3));

drop table event cascade constraints;
create table event (
cid		varchar2(3),
composer	varchar2(10),
work		varchar2(30),
vnum		number(1),
edate		date,
price		number(2));

insert into choir(cid,cname,ctype,csize) values('ca','sarum college','general',24);
insert into choir(cid,cname,ctype,csize) values('cc','cathedral','general',45);
insert into choir(cid,cname,ctype,csize) values('cn','cathedral school','children',30);
insert into music(composer,work,accompaniment) values('js bach','st john passion','chamber');
insert into music(composer,work,accompaniment) values('tallis','spem in alium','none');
insert into music(composer,work,accompaniment) values('tallis','fugue in f minor','none');
insert into music(composer,work,accompaniment) values('stanford','evening canticles','organ');
insert into music(composer,work,accompaniment) values('jc bach','psalm','organ');
insert into music(composer,work,accompaniment) values('js bach','canon','harpsichord');
insert into venue(vnum,location,capacity) values(1,'cathedral',350);
insert into venue(vnum,location,capacity) values(5,'guildhall',150);
insert into venue(vnum,location,capacity) values(8,'st lukes',85);
insert into event(cid,composer,work,vnum,edate,price) values('cc','js bach','st john passion',1,'25-JUL-13',25);
insert into event(cid,composer,work,vnum,edate,price) values('ca','tallis','spem in alium',8,'27-JUL-14',20);
insert into event(cid,composer,work,vnum,edate,price) values('cc','jc bach','psalm',1,'30-JUL-14',16);
insert into event(cid,composer,work,vnum,edate,price) values('ca','tallis','fugue in f minor',5,'28-JUL-14',16);
insert into event(cid,composer,work,vnum,edate,price) values('cn','stanford','evening canticles',8,'28-JUL-14',10);
insert into event(cid,composer,work,vnum,edate,price) values('cc','js bach','st john passion',1,'30-AUG-14',NULL);
insert into event(cid,composer,work,vnum,edate,price) values('cc','js
bach','canon',1,'29-JUL-14',16);
describe choir;
select * from choir;
describe music;
select * from music;
describe venue;
select * from venue;
describe event;
select * from event;

