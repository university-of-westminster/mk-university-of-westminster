/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw02_hotel_oop;

/**
 *
 * @author Martin
 */
public class CW02_Hotel_OOP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hotel hotel = new Hotel(10);
        boolean run = true;
        while (run){
            run = hotel.run();
        }
        System.out.println("Goodbye.");
    }
    
}
