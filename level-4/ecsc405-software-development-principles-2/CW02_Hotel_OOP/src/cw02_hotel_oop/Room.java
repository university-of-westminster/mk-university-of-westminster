/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw02_hotel_oop;

/**
 *
 * @author Martin
 */
public class Room {
    
    private String name;
    
    public Room(){
        this.name = "";
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
}
