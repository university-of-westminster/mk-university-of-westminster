/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw02_hotel_oop;

/**
 *
 * @author Martin
 */
public class Queue {
    
    private String queue[];
    
    public Queue(int size){
        this.queue = new String[size];
        for (int i = 0; i < size; i++){
            this.queue[i] = "";
        }
    }
    
    private int getLastSpot(){
        for (int i = 0; i < this.queue.length; i++){
            if (this.queue[i].equals("")){
                return i;
            }
        }
        return -1;
    }
    
    public boolean push(String s){
        int index = this.getLastSpot();
        if (index < 0){
            System.out.println("Queue is full. First customer is " + this.pop() + " and will be removed.");
            index = this.getLastSpot();
        }
        this.queue[index] = s;
        return true;
    }       
    
    public String pop(){
        String element = this.queue[0];
        for (int i = 0; i < (this.queue.length - 1); i++){
            this.queue[i] = this.queue[i+1];
        }
        this.queue[this.queue.length-1] = "";
        return element;
    }
    
    public boolean avail(){
        return (this.getLastSpot() != 0);
    }
    
}
