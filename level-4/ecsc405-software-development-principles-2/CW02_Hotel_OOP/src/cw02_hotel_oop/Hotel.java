/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw02_hotel_oop;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
public class Hotel {
    
    private Room room[];
    private Queue q = new Queue(7);
    private Scanner in = new Scanner(System.in);
    
    public Hotel(int maxRooms){
        this.room = new Room[maxRooms];
        for (int i =0; i < maxRooms; i++){
            this.room[i] = new Room();
        }
       //this.fillRandom();
    }
    
    public boolean run(){
        if (this.selection()){
            this.pause();
            return true;
        } else{
            return false;
        }
    }
    
    private boolean selection(){
        System.out.println("|----------------------------------------------|");
        System.out.println("|Available options:                            |");
        System.out.println("|A: Add customer                               |");
        System.out.println("|D: Remove customer                            |");
        System.out.println("|V: View all rooms                             |");
        System.out.println("|3: First 3 customers allocated to a room      |");
        System.out.println("|S: Save hotel                                 |");
        System.out.println("|L: Load hotel                                 |");
        System.out.println("|X: Exit program                               |");
        System.out.println("|----------------------------------------------|");
        System.out.print("Choice: ");
        switch (in.next().toUpperCase()){
            case "A":
                this.addCustomer();
                return true;
            case "D":
                this.removeCustomer();
                return true;
            case "V":
                this.viewAll();
                return true;
            case "3":
                this.popQueue();
                return true;
            case "S":
                this.save();
                return true;
            case "L":
                this.load();
                return true;
            case "X":
                return false;
            default:
                System.out.println("Invalid option, try again.");
                return true;
        }
    }
    
    private void addCustomer(){
        int roomNum = 0;
        String name = "";
        int firstIndex = this.findFirstEmpty();
        if (firstIndex == -1){ System.out.println("There are no rooms left."); return; }
        while ((roomNum < 1) || (roomNum > 10)){
            System.out.print("Room number (first empty is " + (firstIndex + 1) + "):");
            roomNum = this.in.nextInt();
            if ((roomNum < 1) || (roomNum > 10)){ System.out.println("Invalid room number, try again."); }
        }
        if (!this.room[roomNum - 1].getName().equals("")){ System.out.println("This room is occupied."); return; }
        while (name.equals("")){
            System.out.print("Customer name: ");
            name = this.in.next();
        }
        this.room[roomNum - 1].setName(name);
        this.q.push(name);
        System.out.println("Customer " + name + " has been added to room " + roomNum);
    }
    
    private void removeCustomer(){
        int roomNum = 0;
        String choice = "";
        while ((roomNum < 1) || (roomNum > 10)){
            System.out.print("Room number to empty: ");
            roomNum = this.in.nextInt();
            if ((roomNum < 1) || (roomNum > 10)){ System.out.println("Invalid room number, try again."); }
        }
        if (this.room[roomNum-1].getName().equals("")){
            System.out.println("This room is empty.");
            return;
        }
        System.out.println("You are emptying room number " + roomNum + " occupied by " + this.room[roomNum - 1].getName() + ". Are you sure? (y/n)");
        while (!(choice.equals("y")) && !(choice.equals("n"))){ choice = this.in.next(); }
        if (choice.equals("n")){
            System.out.println("Aborting...");
        } else if (choice.equals("y")){
            this.room[roomNum - 1].setName("");
            System.out.println("Room " + roomNum + " is now empty.");
        }
    }
    
    private void viewAll(){
        for (int i = 0; i < this.room.length; i++){
            if (this.room[i].getName().equals("")){
                System.out.println("Room " + (i + 1) + " is empty.");
            } else{
                System.out.println("Room " + (i + 1) + " is occupied by " + this.room[i].getName());
            }
        }
    }
    
    private void popQueue(){
        System.out.println("First 3 customers allocated to a room:");
        if (!this.q.avail()){ System.out.println("NONE."); }
        for (int i = 0; i < 3; i++){
            if (this.q.avail()){
                System.out.println(this.q.pop());
            }
        }
    }
    
    private void save(){
        System.out.print("Save name: ");
        String filename = in.next();
        System.out.println("Saving...");
        try {
            FileWriter w = new FileWriter(filename + ".hsave");
            w.write(this.room.length + " ");
            for (int i = 0; i < this.room.length; i++){
                w.write(this.room[i].getName() + " ");
            }
            w.close();
        } catch (IOException ex) {
            Logger.getLogger(CW02_Hotel_OOP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void load(){
        System.out.print("Save name: ");
        String filename = in.next();
        System.out.println("Loading...");
        try {
            Scanner r = new Scanner(new BufferedReader(new FileReader((filename + ".hsave"))));
            this.room = new Room[r.nextInt()];
            for (int i = 0; i < this.room.length; i++){ this.room[i] = new Room(); }
            int x = 0;
            while (r.hasNext()){
                this.room[x].setName(r.next());
                System.out.println("Loaded room " + (x + 1) + ": " + this.room[x].getName());
                x++;
            }
            r.close();
        } catch (IOException ex) {
            Logger.getLogger(CW02_Hotel_OOP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private int findFirstEmpty(){
        for (int i = 0; i < this.room.length; i++){
            if (this.room[i].getName().equals("")){
                return i;
            }
        }
        return -1;
    }
    
    private void pause(){
        System.out.print("Press ENTER to continue...");
        try {
            new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException ex) {
            Logger.getLogger(CW02_Hotel_OOP.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(""); System.out.println("");
    }
    
    
    
    
    /******   DEBUG FUNCTIONS ******/
    
    private void fillRandom(){
        for (int i = 0; i < this.room.length; i++){
            if (Math.random() < 0.7){
                this.room[i].setName(this.generateName());
            }
        }
    }
    
    private String generateName(){
        String name = "";
        for (int i = 0; i < (Math.ceil(Math.random()*100)+3); i++){
            name += (char)(Math.floor(Math.random()*(120-67)+67));
        }
        return name;
    }
}
