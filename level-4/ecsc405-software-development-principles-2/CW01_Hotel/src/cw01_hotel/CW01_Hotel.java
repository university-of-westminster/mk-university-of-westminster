/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw01_hotel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
public class CW01_Hotel {

    /**
     * @param args the command line arguments
     */
    
    public static String[] hotel = new String[10];
    public static Scanner in = new Scanner(System.in);
    
    public static void main(String[] args) {
        boolean run = true;
        init();
        fill();
        while (run){
            run = displayMenu();
            if (run){ continuePrompt(); }
        }
        System.out.println("Goodbye.");
    }
    
    public static boolean displayMenu(){
        System.out.println("|----------------------------------------------|");
        System.out.println("|Available options:                            |");
        System.out.println("|A: Add customer                               |");
        System.out.println("|D: Remove customer                            |");
        System.out.println("|V: View all rooms                             |");
        System.out.println("|O: View all rooms ordered by customer name    |");
        System.out.println("|E: View empty rooms                           |");
        System.out.println("|F: Find customer                              |");
        System.out.println("|S: Save hotel                                 |");
        System.out.println("|L: Load hotel                                 |");
        System.out.println("|X: Exit program                               |");
        System.out.println("|----------------------------------------------|");
        System.out.print("Choice: ");
        switch (in.next().toUpperCase()){
            case "A":
                addCustomer();
                return true;
            case "D":
                removeCustomer();
                return true;
            case "V":
                viewAll();
                return true;
            case "O":
                viewAllOrdered();
                return true;
            case "E":
                viewEmpty();
                return true;
            case "F":
                findCustomer();
                return true;
            case "S":
                save();
                return true;
            case "L":
                load();
                return true;
            case "X":
                return false;
            default:
                System.out.println("Invalid option, try again.");
                return true;
        }
    }
    
    public static void init(){
        for (int i = 0; i < 10; i++){
            hotel[i] = "EMPTY";
        }
    }
    
    public static void viewAll(){
        for (int i = 0; i < 10; i++){
            System.out.print("Room " + (i+1) + ": ");
            System.out.println((hotel[i].equals(""))?("EMPTY"):(hotel[i]));
        }
    }
    
    public static void viewAllOrdered(){
        String[] h = hotel.clone();
        int[] r = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        java.util.LinkedList<Integer> e = new LinkedList<>();
        int x = 0;
        while (x < 9){
            if (h[x].compareTo(h[x+1]) > 0){
                String tmp = h[x];
                int tmpr = r[x];
                h[x] = h[x+1];
                h[x+1] = tmp;
                r[x] = r[x+1];
                r[x+1] = tmpr;
                x = -1;
            }
            x++;
        }
        for (int i = 0; i < 10; i++){
            if (h[i].equals("EMPTY")){
                e.add(i);
            } else{
                System.out.print("Room " + r[i] + ": ");
                System.out.println(h[i]);
            }
        }
        for (int i = 0; i < e.size(); i++){
            System.out.println("Room " + r[e.get(i)] + ": EMPTY");
        }
    }
    
    public static void viewEmpty(){
        boolean b = false;
        System.out.println("Empty rooms:");
        for (int i = 0; i <10; i++){
            if (hotel[i].equals("EMPTY")){
                System.out.println("Room " + (i+1));
                b = true;
            }
        }
        if (!b){ System.out.println("NONE"); }
    }
    
    public static void addCustomer(){
        System.out.print("Room number: ");
        int roomnum = in.nextInt();
        if ((roomnum < 1) || (roomnum > 10) || !hotel[roomnum].equals("EMPTY")){
            System.out.println("This room either does not exist or is occupied.");
        } else{
            System.out.print("Customer name: ");
            hotel[roomnum] = in.next();
            System.out.println("Customer " + hotel[roomnum] + " has been added to room " + roomnum);
        }
    }
    
    public static void removeCustomer(){
        int n = -1;
        System.out.print("Room number: ");
        n = in.nextInt();
        if ((n > 0) && (n < 11)){
            System.out.println("Emptying room number " + n + ", occupied by: " + hotel[n-1] + ".");
            hotel[n-1] = "EMPTY";
        } else{
            System.out.println("Room does not exist.");
        }
    }
    
    public static void findCustomer(){
        String name;
        boolean f = false;
        System.out.print("Customer name: ");
        name = in.next().toLowerCase();
        for (int i = 0; i < 10; i++){
            if (hotel[i].toLowerCase().equals(name)){
                System.out.println("Customer " + hotel[i] + " found in room " + (i+1) + ".");
                f = true;
            }
        }
        if (!f){ System.out.println("Noone with this name is currently occupying the hotel."); }
        
    }
    
    public static void save(){
        System.out.print("Save name: ");
        String filename = in.next();
        System.out.println("Saving...");
        try {
            FileWriter w = new FileWriter(filename + ".hsave");
            for (int i = 0; i < 10; i++){
                w.write(hotel[i] + " ");
            }
            w.close();
        } catch (IOException ex) {
            Logger.getLogger(CW01_Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void load(){
        System.out.print("Save name: ");
        String filename = in.next();
        System.out.println("Loading...");
        try {
            Scanner r = new Scanner(new BufferedReader(new FileReader((filename + ".hsave"))));
            int x = 0;
            while (r.hasNext()){
                hotel[x] = r.next();
                System.out.println("Loaded room " + x + ": " + hotel[x]);
                x++;
            }
            r.close();
        } catch (IOException ex) {
            Logger.getLogger(CW01_Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public static void continuePrompt(){
        System.out.print("Press ENTER to continue...");
        try {
            new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException ex) {
            Logger.getLogger(CW01_Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(""); System.out.println("");
    }
    
    public static void fill(){
        hotel[0] = "Xerox";
        hotel[1] = "Hank";
        hotel[2] = "Kyle";
        //hotel[3] = "Sierra";
        hotel[4] = "Mark";
        hotel[5] = "Lindsay";
       // hotel[6] = "Indy";
        hotel[7] = "Karmen";
        hotel[8] = "Freddy";
       // hotel[9] = "Onyx";
    }
    
}
